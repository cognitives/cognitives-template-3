# TODO: automate the process of storing minified files
# TODO: allow paths to be overwritten
# TODO: Find a way to have index.coffee update on watch

gulp       = require "gulp"
yargs      = require "yargs"
del        = require "del"
bower      = require "gulp-bower"
coffee     = require "gulp-coffee"
concat     = require "gulp-concat"
flatten    = require "gulp-flatten"
gulpif     = require "gulp-if"
htmlclean  = require "gulp-htmlclean"
imagemin   = require "gulp-imagemin"
livereload = require "gulp-livereload"
order      = require "gulp-order"
plumber    = require "gulp-plumber"
sass       = require "gulp-ruby-sass"
sizereport = require "gulp-sizereport"
sourcemaps = require "gulp-sourcemaps"
uglify     = require "gulp-uglify"
uglifycss  = require "gulp-uglifycss"
unzip      = require "gulp-unzip"
watch      = require "gulp-watch"

regex_all = "/**/*"


config = require "../html/config.coffee"

theme_name = config.theme_name or "Local News"
source_url = config.source_url or "../html"
output_url = config.output_url or "../../wordpress"

# TODO: override the paths with config
paths =
  fonts:
    src: ["#{source_url}/assets/fonts/#{regex_all}"]
    dest: "#{output_url}/static/fonts/"
  images:
    src: ["#{source_url}/assets/images/#{regex_all}"]
    dest: "#{output_url}/static/images/"
  php:
    src: "#{source_url}/php/"
    libraries_src: ["#{source_url}/php_libraries/#{regex_all}.*"]
    dest: output_url
  js:
    app:    "#{source_url}/assets/javascripts/coffee/index.coffee"
    base:   "#{source_url}/assets/javascripts/js"
    coffee: "#{source_url}/assets/javascripts/coffee/"
  css:
    base: "#{source_url}/assets/stylesheets/"
    sass: "#{source_url}/assets/stylesheets/sass"
    bootstrap: ["./bower_components/bootstrap-sass/assets/stylesheets"]


# Flag for compiling theme with production settings
production = !!(yargs.argv.production)


process.stdout.write("\n\n==================================================\n")
process.stdout.write("Theme Name: #{theme_name}\n")
process.stdout.write("Compiling for Production\n") if production
process.stdout.write("==================================================\n\n\n")


gulp.task "init", ->
  #TODO: generate the base assets folder
  #TODO: pull a copy of the latest wordpress and setup the folder

  # Set up bower to obtain css/javascript libraries
  bower().pipe gulp.dest("./bower_components")

gulp.task "images", ->
  gulp.src paths.images.src
    # Optimize images
    .pipe gulpif(production, imagemin(progressive: true))
    .pipe gulp.dest(paths.images.dest)

gulp.task "fonts", ->
  gulp.src paths.fonts.src
    .pipe gulp.dest(paths.fonts.dest)

gulp.task "php", ->
  gulp.src "#{paths.php.src}/#{regex_all}"
    # Remove whitespace in the rendered HTML aspect in the PHP
    .pipe gulpif(production, htmlclean())
    .pipe gulp.dest(paths.php.dest)
    .pipe livereload()

gulp.task "php_libraries", ->
  gulp.src paths.php.libraries_src
    .pipe gulp.dest("#{paths.php.dest}/php_libraries")

gulp.task "js", (cb) ->
  # Compile all coffeescripts into javascript
  gulp.src "#{paths.js.coffee}/#{regex_all}.coffee"
    .pipe coffee(bare: true)
    .pipe gulp.dest(paths.js.base)

  # Concat and minify all javascript files
  js_files = ("#{paths.js.base}/#{file_name}.js" for file_name in require(paths.js.app))

  gulp.src js_files
    .pipe concat("index.js")
    .pipe gulpif(production, uglify())
    .pipe gulp.dest("#{paths.php.dest}/static/js")
    .pipe sizereport(gzip: true, total: false)
    .pipe livereload()

  # Internet explorer javascript
  gulp.src ["#{paths.js.base}/ie.js"]
    .pipe gulpif(production, uglify())
    .pipe gulp.dest("#{paths.php.dest}/static/js/ie")

  #dropzone files
  #gulp.src(["#{paths.js.base}/dropzone/#{regex_all}"])
   # .pipe gulp.dest("#{paths.php.dest}/static/js/dropzone")

gulp.task "css", ->
  # Load sass frameworks
  sass(paths.css.sass, loadPath: [paths.css.bootstrap], sourcemap: !production)
    # Include the sourcemaps for debugging
    .pipe gulpif(!production, sourcemaps.write())
    .pipe gulpif(production, uglifycss())
    .pipe gulp.dest("#{paths.php.dest}/static/css")
    .pipe sizereport(gzip: true, total: false)
    .pipe livereload()

gulp.task "minify_js", ->
  # TODO: List file sizes
  gulp.src "#{paths.js.base}/*.js"
    .pipe uglify()
    .pipe gulp.dest("#{paths.js.base}/../min")


gulp.task "clean", (cb) ->

  # Clear out all folders in the theme
  del.sync(["#{output_url}/images/**", "#{output_url}/images", "#{output_url}/*", output_url], force: true)
  cb()

gulp.task "default", ->
  gulp.start "clean"
  gulp.start "images", "fonts", "php", "php_libraries", "js", "css"

gulp.task "watch", ->
  gulp.start "default"

  livereload.listen()

  watch paths.images.src, -> gulp.start "images"
  watch paths.php.src,    -> gulp.start "php"
  watch paths.js.coffee,  -> gulp.start "js"
  watch paths.js.app,     -> gulp.start "js"
  watch paths.css.sass,   -> gulp.start "css"
