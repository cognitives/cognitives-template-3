module.exports = [
  # jQuery
  "jquery-2.1.0.min"
  "jquery-migrate-1.2.1.min"
  "bootstrap.min"
  #"jquery-1.11.3.min"

  # "jquery.browser"

  # NAVIGATION

  # "jquery.headshrinker"                         # Fixed navigation header
  # "jquery.scrollup"                             # Scroll to top arrow
  # "jquery.slicknav"                             # Responsive navigation
  "jquery.bigslide.min"                               # Side slide in navigation
  # "jquery.dropit"                                 # Dropdown menu
  # "jquery.tabs"


  # Utilities
  # "jquery.lazy"                                 # Delays the loading of images only when they come into view
  "jquery.simplemodal"
  # "jquery.slider"
  # "jquery.loading"                              # Loading screen to hide partially rendered page


  # IMAGES
  "jquery.slick.min"                              # Images carousel
  # "jquery.magnific-popup"                       # Lightbox
  # "jquery.justifiedgallery.min"


  # ANIMATION
  # GSAP
  # "gsap/jquery.gsap.min"
  # # "gsap/TweenLite.min"
  # # "gsap/plugins/CSSPlugin.min"
  # "gsap/TweenMax.min"
  # # "gsap/TimelineLite.min"
  # # "gsap/TimelineMax.min"

  # # Scroll Magic
  # "scroll-magic/ScrollMagic.min"
  # "scroll-magic/jquery.ScrollMagic.min"
  # "scroll-magic/animation.gsap.min"
  # # "scroll-magic/debug.addIndicators.min"


  # GOOGLE MAPS
  # "jquery.lazy-load-google-maps"
  # "map-styles"
  # "maps"


  # TEXT MANIPULATION

  "jquery.dotdotdot"



  # APPLICATION

  # "helper"
  "application"

  "owl.carousel"
]
