var cardHolder, handleFullScreen, handleSearch;

$.resizeSections = function() {
  var $height, $sections, $window;
  $window = $(window);
  $sections = $(".full_screen");
  $height = $window.height();
  $sections.each(function() {
    return $(this).css("min-height", $height + "px");
  });
  return $window.on("resize", function() {
    $height = $window.height();
    return $sections.each(function() {
      return $(this).css("min-height", $height + "px");
    });
  });
};

$(document).ready(function() {
  $.resizeSections();
  $(".slides").slick({
    arrows: false,
    autoplay: true,
    autoplaySpeed: 3000,
    dots: true,
    draggable: false,
    slidesToShow: 1,
    speed: 400
  });
  return $("#profile").on("click", function(e) {
    e.preventDefault();
    return $("#header__menu").simplemodal({
      modal: true,
      overlayClose: true,
      opacity: 0.9,
      fixed: false,
      zIndex: 11010,
      closeHTML: "",
      autoPosition: false,
      onShow: function(object) {
        var header;
        header = $(".header__container").outerHeight();
        return $(object.container).css("top", header + "px");
      }
    });
  });
});

handleSearch = function() {
  $('#custom-search-button').on('click', function(e) {
    e.preventDefault();
    if (!$('#custom-search-button').hasClass('open')) {
      $('#custom-search-form-container').fadeIn(600);
    } else {
      $('#custom-search-form-container').fadeOut(600);
    }
  });
  $('#custom-search-form').append('<a class="custom-search-form-close" href="javascript:;" title="Close Search Box"><span>x</span></a>');
  $('#custom-search-form-container a.custom-search-form-close').on('click', function(event) {
    event.preventDefault();
    $('#custom-search-form-container').fadeOut(600);
  });
};

handleFullScreen = function() {
  var x;
  x = $(window).height();
  $('.fullscreen').css('height', x + 'px');
};

$(document).ready(function() {
  handleFullScreen();
  handleSearch();
});

$(window).resize(function() {
  handleFullScreen();
});

$(document).ready(function() {
  var $back, $navigation, $nextItem;
  $navigation = '.responsive-navigation';
  $nextItem = '<span class="next-item"><i class="fa fa-angle-right"></i></span>';
  $back = '<li class="back"><a href="javascript:;"><i class="fa fa-chevron-left"></i>Back</a></li>';
  $(".responsive-navigation .responsive-navigation__list li:has(ul)").prepend($nextItem);
  $(".responsive-navigation .responsive-navigation__list .sub-menu").prepend($back);
  $('#header-responsive').on('click', function() {
    $('body').addClass('no-scroll');
    $('.responsive-navigation').addClass('navigation-active');
  });
  $('.responsive-navigation .next-item').on('click', function(e) {
    e.preventDefault();
    $(this).nextAll('.sub-menu').addClass('navigation-active');
  });
  $('.responsive-navigation .back').on('click', function(e) {
    e.preventDefault();
    $(this).parent('.sub-menu').removeClass('navigation-active');
  });
  $('.close-menu').on('click', function() {
    $('.responsive-navigation').removeClass('navigation-active');
    $('body').removeClass('no-scroll');
  });
  $('.menu-overlay').on('click', function() {
    $('.responsive-navigation').removeClass('navigation-active');
    $('body').removeClass('no-scroll');
  });
  $(".header__navigation-list li:has(ul)").addClass("menu-item-has-children");
  $(".header__navigation-list li:has(ul) > a").addClass("has-child");
});

$('#videoModal').on('hide.bs.modal', function(e) {
  var $if, src;
  $if = $(e.delegateTarget).find('iframe');
  src = $if.attr('src');
  $if.attr('src', '/empty.html');
  $if.attr('src', src);
});

$('#AjaxLoginModal').on('show.bs.modal', function(e) {
  var link;
  link = $(e.relatedTarget);
  $(this).find('.modal-body').load(link.attr('href'));
});

$('#AjaxUserProfileModal').on('show.bs.modal', function(e) {
  var link;
  link = $(e.relatedTarget);
  $(this).find('.modal-body').load(link.attr('href'));
});

$('#AjaxMyPlatformModal').on('show.bs.modal', function(e) {
  var link;
  link = $(e.relatedTarget);
  $(this).find('.modal-body').load(link.attr('href'));
});

$(".category_share_icon").hover((function() {
  return $(this).find(".tooltip").addClass("tooltip--visible");
}), function() {
  return $(this).find(".tooltip").removeClass("tooltip--visible");
});

$(".social_share_toggler").on("click", function(e) {
  e.preventDefault();
  $(".social_share_toggler").toggleClass("selected");
});

cardHolder = '';

$(window).load(function() {
  clearTimeout(cardHolder);
  cardHolder = setTimeout((function() {
    $(".card p, .card h1").dotdotdot({
      watch: true
    });
  }), 750);
});
