$.anchorScroll = function(section_name) {
  var id;
  section_name = section_name ? section_name : window.location.hash.replace("#", "");
  id = $("#section-" + section_name);
  if (id.length > 0) {
    return $("html, body").animate({
      scrollTop: id.offset().top
    }, 400);
  }
};

$.isMobile = function() {
  return $(window).width() < 480;
};

$.animation = function() {
  var $window, border, controller, scene, slides, tl;
  $("a.social_icon").hover((function() {
    return TweenMax.to($(this), 0.05, {
      scale: 1.3
    });
  }), function() {
    return TweenMax.to($(this), 0.5, {
      scale: 1
    });
  });
  $("#slides__arrow, .section__arrow").hover((function() {
    return TweenMax.to($(this), 0.05, {
      y: 5
    });
  }), function() {
    return TweenMax.to($(this), 0.5, {
      y: 0
    });
  });
  $("#footer__arrow").hover((function() {
    return TweenMax.to($(this), 0.05, {
      y: -5
    });
  }), function() {
    return TweenMax.to($(this), 0.5, {
      y: 0
    });
  });
  slides = $(".slide");
  if (slides.length > 0) {
    border = parseInt($(".section").css("border-top-width").replace("px", "")) * 2;
    $window = $(window);
    slides.height($window.height() - border);
    $window.on("resize", function() {
      return slides.height($window.height() - border);
    });
  }
  $(".section").parallax();
  controller = new ScrollMagic.Controller();
  if ($.isMobile()) {
    $(".header").addClass("header__fixed");
  } else {
    scene = new ScrollMagic.Scene({
      triggerElement: $(".section").not(".section:first"),
      triggerHook: "onLeave",
      offset: -80
    });
    scene.setClassToggle($(".header"), "header__fixed");
    scene.addTo(controller);
  }
  $(".slide").each(function() {
    var tl;
    scene = new ScrollMagic.Scene({
      triggerElement: $(this),
      triggerHook: "onLeave",
      duration: "50%",
      offset: 100
    });
    tl = new TimelineLite();
    tl.addLabel("start");
    tl.add(TweenMax.to($(this).find(".slide__heading"), 1, {
      scale: 0.7,
      opacity: 0,
      y: -200,
      ease: Linear.easeNone
    }), "start");
    tl.add(TweenMax.to($(this).find(".slide__subheading"), 1, {
      scale: 0.9,
      opacity: 0,
      y: -250,
      ease: Linear.easeNone
    }), "start");
    tl.add(TweenMax.to($(this).find(".slide__button"), 1, {
      opacity: 0,
      y: -250,
      ease: Linear.easeNone
    }), "start");
    scene.setTween(tl);
    return scene.addTo(controller);
  });
  $(".heading").each(function() {
    var tl;
    scene = new ScrollMagic.Scene({
      triggerElement: $(this),
      triggerHook: "onLeave",
      duration: "50%"
    });
    tl = new TimelineLite();
    tl.addLabel("start");
    tl.add(TweenMax.to($(this).find(".heading__heading"), 1, {
      opacity: 0,
      y: -300,
      ease: Linear.easeNone
    }), "start");
    tl.add(TweenMax.to($(this).find(".heading__subheading"), 1, {
      opacity: 0,
      y: -500,
      ease: Linear.easeNone
    }), "start");
    scene.setTween(tl);
    return scene.addTo(controller);
  });
  scene = new ScrollMagic.Scene({
    triggerElement: $(".footer"),
    triggerHook: "onCenter"
  });
  tl = new TimelineLite();
  tl.add(TweenMax.staggerFrom($(".footer").find(".footer__content .container"), 0.3, {
    opacity: 0,
    y: 50,
    ease: Power2.easeOut
  }));
  tl.add(TweenMax.staggerFrom($(".footer").find(".footer__copyright .container"), 0.2, {
    opacity: 0,
    y: 50,
    ease: Power2.easeOut
  }));
  scene.setTween(tl);
  return scene.addTo(controller);
};

$.fn.parallax = function(controller) {
  var windowHeight;
  if ($.browser.msie) {
    return;
  }
  if ($.isMobile()) {
    return;
  }
  windowHeight = $(window).height();
  controller = new ScrollMagic.Controller();
  return $(this).each(function() {
    var background, element, scene, timeline;
    element = $(this);
    background = $(this).find(".background");
    background.height(element.height() + (element.height() * .2));
    background.css("top", "-60%");
    $(window).on("resize", function() {
      background.height(element.height() + (element.height() * .2));
      return background.css("top", "-60%");
    });
    scene = new ScrollMagic.Scene({
      triggerElement: element,
      triggerHook: "onEnter",
      duration: "200%"
    });
    timeline = TweenMax.to(background, 1, {
      y: "100%",
      ease: Linear.easeNone
    });
    scene.setTween(timeline);
    return scene.addTo(controller);
  });
};

$.fn.nextSection = function() {
  return $(this).each(function() {
    return $(this).on("click", function(e) {
      var id;
      e.preventDefault();
      id = $(this).parents(".section").next();
      if (id.length > 0) {
        return $("html, body").animate({
          scrollTop: id.offset().top
        }, 400);
      }
    });
  });
};
