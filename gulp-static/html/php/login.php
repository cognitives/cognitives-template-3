<?php require "partials/_header.php"; ?>

<div class="login-page">
    <div class="container">
      <div class="row">
        <div class="col-md-11 col-xs-12 col-centered">
          <div class="tabbable tabbable-custom">
              <ul class="nav nav-tabs nav_tabs_two">
                  <li class="active"><a href="#Login" data-toggle="tab">Login</a></li>
                  <li class=""><a href="#SignUp" data-toggle="tab">Sign Up</a></li>
              </ul>
              <div class="tab-content">
                  <div class="tab-pane fade in active" id="Login">
                      <div class="row">
                          <div class="col-md-12 col-sm-12 col-xs-12" id="login-social">
                              <div class="social-buttons">
                                  <ul class="row">
                                      <li class="facebook"><a href="#"><i class="fa fa-facebook"></i> Login with Facebook</a></li>
                                      <li class="twitter"><a href="#"><i class="fa fa-twitter"></i> Login with Twitter</a></li>
                                      <li class="g-plus"><a href="#"><i class="fa fa-google-plus"></i> Login with Google+</a></li>
                                      <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i> Login with LinkedIn</a></li>
                                  </ul>
                              </div>
                              <p class="text-center mbt50"><a href="#" class="txt-link">Don't have an account? <span class="grey_themer">Sign up here</span></a></p>

                              <p class="email-login"><span class="seperation">Login with email</span></p>
                              <div class="WriteAnArticleForm">
                                  <form name="" class="form-horizontal" action="#" method="post">
                                      <div class="alert alert-danger alert-dismissible" role="alert">
                                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                          </button>
                                          <ul>
                                            <li>First name cannot be blank.</li>
                                            <li>Last name cannot be blank.</li>
                                            <li>Email cannot be blank.</li>
                                            <li>Username cannot be blank.</li>
                                            <li>Password cannot be blank.</li>
                                            <li>Verify password cannot be blank.</li>
                                            <li>Captcha cannot be blank.</li>
                                          </ul>
                                      </div>

                                      <div class="controls-full"><input type="text" placeholder="Email" class="form-control error" name="Email">
                                          <label id="username-error" class="error" for="username">Username cannot be blank.</label>
                                      </div>
                                      <div class="controls-full"><input type="password" placeholder="Password" class="form-control success" name="Password">
                                          <label id="username-error" class="success" for="username">Username cannot be blank.</label>
                                      </div>


                                      <div class="button-set">
                                          <label><input type="checkbox" id="RemeberMe" name="RemeberMe" value="RemeberMe"><span><!-- fake checkbox --></span>Remeber Me</label>
                                          <div class="right-section">
                                            <a href="forgot.php" class="forget"><span class="grey_themer green">Forgot Password?</span></a>
                                            <button type="submit" class="button grey button-large">Login</button>
                                          </div>
                                      </div>

                                  </form>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="tab-pane fade in" id="SignUp">
                      <div class="row">
                          <div class="col-md-12 col-sm-12 col-xs-12" id="signup-social">

                              <p class="email-login"><span class="seperation">Let’s get this party started!</span></p>

                              <div class="social-buttons">
                                  <ul class="row">
                                      <li class="facebook"><a href="#"><i class="fa fa-facebook"></i> Sign up with Facebook</a></li>
                                      <li class="twitter"><a href="#"><i class="fa fa-twitter"></i> Sign up with Twitter</a></li>
                                      <li class="g-plus"><a href="#"><i class="fa fa-google-plus"></i> Sign up with Google+</a></li>
                                      <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i> Sign up with LinkedIn</a></li>
                                  </ul>
                              </div>
                              <p class="text-center mbt50"><a href="#" class="txt-link">Already have an account? <span class="grey_themer">Login here</span></a></p>

                              <p class="email-login"><span class="seperation">Sign up with email</span></p>
                              <div class="WriteAnArticleForm">
                                  <form name="" class="form-horizontal" action="#" method="post">
                                      <div class="alert alert-danger alert-dismissible" role="alert">
                                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                          <strong>Warning!</strong> Better check yourself, you're not looking too good.
                                      </div>

                                      <div class="controls-lt"><input type="text" placeholder="First name" class="form-control error" name="FirstName">
                                          <label id="username-error" class="error" for="username">Username cannot be blank.</label>
                                      </div>
                                      <div class="controls-rt"><input type="text" placeholder="Last name" class="form-control success" name="LastName">
                                          <label id="username-error" class="success" for="username">Username cannot be blank.</label>
                                      </div>
                                      <div class="controls-full"><input type="text" placeholder="Email" class="form-control" name="Email"></div>
                                      <div class="controls-full"><input type="password" placeholder="Password" class="form-control" name="Password"></div>
                                      <div class="controls-full"><input type="password" placeholder="Verify password" class="form-control" name="VerifyPassword"></div>
                                      <div class="controls-full"><input type="text" placeholder="Fans Unite username" class="form-control" name="Username"></div>

                                      <div class="button-set">
                                          <label><input type="checkbox" id="AcceptTerms" name="AcceptTerms" value="AcceptTerms"><span><!-- fake checkbox --></span>I accept terms of use.</label>
                                          <div class="right-section">
                                              <button type="submit" class="button grey button-large">Sign up</button>
                                          </div>
                                      </div>
                                  </form>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <!-- //End Sign Up -->
</div>


<!-- Begin Ajax based My Platform Modal Popup -->
<div class="modal fade" id="AjaxMyPlatformModal" tabindex="-1" role="dialog" aria-labelledby="AjaxMyPlatformModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<button type="button" class="modal-close x-close" data-dismiss="modal"><span class="sr-only">Close</span></button>

			<div class="modal-body nopadd"></div>
		</div>
	</div>
</div>
<!-- //End Ajax based Profile Modal Popup -->

<?php require "partials/_footer.php"; ?>
