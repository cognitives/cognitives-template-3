<?php require("partials/_header.php") ?>
<div class="page_section cards_main">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="col-third">
                    <a href="#" class="card card__twitter content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-third">
                    <a href="#" class="card card__news content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-third">
                    <a href="#" class="card card__facebook content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-third">
                    <a href="#" class="card card__instagram content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-third">
                    <a href="#" class="card card__youtube content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-third">
                    <a href="#" class="card card__news ad_icon content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-quarter">
                    <a href="#" class="card card__twitter content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-quarter">
                    <a href="#" class="card card__facebook content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-quarter">
                    <a href="#" class="card card__news ad_icon content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-quarter">
                    <a href="#" class="card card__instagram content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-half">
                    <a href="#" class="card card__facebook content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-half">
                    <a href="#" class="card card__instagram content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-half">
                    <a href="#" class="card card__youtube content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-half">
                    <a href="#" class="card card__news ad_icon content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-two-thirds">
                    <a href="#" class="card card__facebook content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-two-thirds">
                    <a href="#" class="card card__instagram content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-two-thirds">
                    <a href="#" class="card card__youtube content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-two-thirds">
                    <a href="#" class="card card__news ad_icon content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-full">
                    <a href="#" class="card card__twitter content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-full">
                    <a href="#" class="card card__facebook content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-full">
                    <a href="#" class="card card__news ad_icon content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-full">
                    <a href="#" class="card card__youtube content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-full">
                    <a href="#" class="card card__vimeo content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-full">
                    <a href="#" class="card card__instagram content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require("partials/_footer.php") ?>
