<?php require("partials/_header.php") ?>
<div class="page_section cards_main">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="col-third">
                    <a href="#" class="card card__twitter without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-third">
                    <a href="#" class="card card__news without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-third">
                    <a href="#" class="card card__facebook without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-third">
                    <a href="#" class="card card__instagram without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-third">
                    <a href="#" class="card card__youtube without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-third">
                    <a href="#" class="card card__news ad_icon without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-quarter">
                    <a href="#" class="card card__twitter without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-quarter">
                    <a href="#" class="card card__facebook without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-quarter">
                    <a href="#" class="card card__news ad_icon without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-quarter">
                    <a href="#" class="card card__instagram without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-half">
                    <a href="#" class="card card__facebook without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-half">
                    <a href="#" class="card card__instagram without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-half">
                    <a href="#" class="card card__youtube without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-half">
                    <a href="#" class="card card__news ad_icon without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-two-thirds">
                    <a href="#" class="card card__facebook without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-two-thirds">
                    <a href="#" class="card card__instagram without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-two-thirds">
                    <a href="#" class="card card__youtube without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-two-thirds">
                    <a href="#" class="card card__news ad_icon without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-full">
                    <a href="#" class="card card__twitter without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-full">
                    <a href="#" class="card card__facebook without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-full">
                    <a href="#" class="card card__news ad_icon without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-full">
                    <a href="#" class="card card__youtube without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-full">
                    <a href="#" class="card card__vimeo without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-full">
                    <a href="#" class="card card__instagram without__image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require("partials/_footer.php") ?>
