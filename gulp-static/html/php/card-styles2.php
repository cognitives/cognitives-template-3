<?php require("partials/_header.php") ?>
<div class="page_section cards_main">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="col-quarter-short">
                    <a href="#" class="card card__news video_card withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                    <a href="#" class="card card__twitter withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                    <a href="#" class="card card__youtube withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-quarter-short">
                    <a href="#" class="card card__news withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                    <a href="#" class="card card__twitter withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                    <a href="#" class="card card__youtube withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-quarter-short">
                    <a href="#" class="card card__news withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                    <a href="#" class="card card__twitter withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                    <a href="#" class="card card__youtube withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-quarter-short">
                    <a href="#" class="card card__news ad_icon withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                    <a href="#" class="card card__twitter withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                    <a href="#" class="card card__youtube withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-third">
                    <a href="#" class="card card__twitter withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-third">
                    <a href="#" class="card card__news withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-third">
                    <a href="#" class="card card__facebook withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-third">
                    <a href="#" class="card card__instagram withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-third">
                    <a href="#" class="card card__youtube withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-third">
                    <a href="#" class="card card__news ad_icon withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-quarter">
                    <a href="#" class="card card__twitter withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-quarter">
                    <a href="#" class="card card__facebook withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-quarter">
                    <a href="#" class="card card__news ad_icon withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-quarter">
                    <a href="#" class="card card__instagram withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-half">
                    <a href="#" class="card card__facebook withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-half">
                    <a href="#" class="card card__instagram withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-half">
                    <a href="#" class="card card__youtube withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-half">
                    <a href="#" class="card card__news ad_icon withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-two-thirds">
                    <a href="#" class="card card__facebook withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-third-short">
                    <a href="#" class="card card__news ad_icon withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                    <a href="#" class="card card__twitter withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                    <a href="#" class="card card__youtube withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>

                <div class="col-two-thirds">
                    <a href="#" class="card card__instagram withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-two-thirds">
                    <a href="#" class="card card__youtube withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-two-thirds">
                    <a href="#" class="card card__news ad_icon withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-full">
                    <a href="#" class="card card__twitter withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-full">
                    <a href="#" class="card card__facebook withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-full">
                    <a href="#" class="card card__news ad_icon withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-full">
                    <a href="#" class="card card__youtube withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-full">
                    <a href="#" class="card card__vimeo withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-full">
                    <a href="#" class="card card__instagram withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require("partials/_footer.php") ?>
