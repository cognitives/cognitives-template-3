<?php require "partials/_header.php"; ?>

<!-- Begin Container -->
<div class="page_section">
    <section class="container min-hight">
        <div class="row" id="userprofile-page">
            <!-- Begin User Profile -->
            <div class="col-md-4 col-sm-4 col-xs-12">

            <!-- Begin User Profile Sidebar -->

            <div id="userprofile-sidebar">
                <figure class="user-profile-pic" style="background-image:url(https://fu-uploads-uat.s3.amazonaws.com/2015/Nov/11/large_cM4hCnZcGvoeE5MmXij5.jpg);"></figure>
                <div class="inner-descr">
                    <h4 class="user-name text-center">Country News Editor</h4>
                    <a href="/profile/admin" class="button selected button-block grey" data-id="1" data-status="follow"><i class="fa fa-star"></i>Follow</a>
                    <p></p>
                    <div class="social-stuffs text-center"> <a href="mailto:rohitg@ideoris.com.au"><i class="fa fa-envelope"></i></a> </div>
                </div>
            </div>
            <!-- //End User Profile Sidebar -->
        </div>
            <div class="col-md-8 col-sm-8 col-xs-12">

                <!-- Begin My Blogs -->
                <div class="polpular-writers">
                <header class="sub-header">My Sections</header>
                <ul>
                    <li><a class="no-image" href="http://v2dev.fansunite.com.au" target="_blank">
                        <button type="button" class="like selected forceLogin" aria-label="Like"><span aria-hidden="true"><i class="fa fa-star"></i></span></button>
                        <div class="img-thumbnail"><img class="img-responsive" src="https://fu-uploads-uat.s3.amazonaws.com/2015/Nov/09/thumb_qUiRFAdphJMAoEgrlw2t.jpg" alt=""></div>
                        <div class="description">
                            <h4>Country News</h4>
                            <p>v2dev.fansunite.com.au</p>
                        </div>
                    </a></li>
                    <li><a href="http://dairy.fansunite.com.au" target="_blank">
                        <button type="button" class="like forceLogin" aria-label="Like"><span aria-hidden="true"><i class="fa fa-star"></i></span></button>
                        <div class="img-thumbnail"><img class="img-responsive" src="/static/resources/images/blog_default_thumbnail.jpg" alt=""></div>
                        <div class="description">
                            <h4>Dairy</h4>
                            <p>dairy.fansunite.com.au</p>
                        </div>
                    </a></li>
                    <li><a href="http://shepparton.fansunite.com.au" target="_blank">
                        <button type="button" class="like forceLogin" aria-label="Like"><span aria-hidden="true"><i class="fa fa-star"></i></span></button>
                        <div class="img-thumbnail"><img class="img-responsive" src="/static/resources/images/blog_default_thumbnail.jpg" alt=""></div>
                        <div class="description">
                            <h4>Shepparton</h4>
                            <p>shepparton.fansunite.com.au</p>
                        </div>
                    </a></li>
                    <li><a href="http://workingdogs.fansunite.com.au" target="_blank">
                        <button type="button" class="like forceLogin" aria-label="Like"><span aria-hidden="true"><i class="fa fa-star"></i></span></button>
                        <div class="img-thumbnail"><img class="img-responsive" src="https://fu-uploads-uat.s3.amazonaws.com/2015/Nov/17/thumb_scb35nyzkpu1cNWexTQY.jpg" alt=""></div>
                        <div class="description">
                            <h4>Working Dogs</h4>
                            <p>workingdogs.fansunite.com.au</p>
                        </div>
                    </a></li>
                </ul>
            </div>

                <!-- //End My Blogs -->

                <!-- Begin Recent Articles -->
                <div class="recent-articles">
                <header class="sub-header">Recent Articles<span class='hidden-xs'> (<a href='/profile/admin/posts' class='view-more'>View all articles</a>)</span></header>
                <div id="news-profile">
                    <div class="section__content">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div id="owl-thumbnails" class="owl-carousel">
                                    <div class="col-third"><?php require 'partials/cards/_facebook-01.php'; ?></div>
                                    <div class="col-third"><?php require 'partials/cards/_facebook-01.php'; ?></div>
                                    <div class="col-third"><?php require 'partials/cards/_facebook-01.php'; ?></div>
                                    <div class="col-third"><?php require 'partials/cards/_facebook-01.php'; ?></div>
                                    <div class="col-third"><?php require 'partials/cards/_facebook-01.php'; ?></div>
                                    <div class="col-third"><?php require 'partials/cards/_facebook-01.php'; ?></div>
                                    <div class="col-third"><?php require 'partials/cards/_facebook-01.php'; ?></div>
                                    <div class="col-third"><?php require 'partials/cards/_facebook-01.php'; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                <!-- //End Recent Articles -->

                <!-- Begin Following -->
                <div class="daily-ten clearfix">
                    <header class="sub-header">Following</header>
                    <div class="tabbable tabbable-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#FollowingCodes" data-toggle="tab">Topics</a></li>
                            <li><a href="#FollowingClubs" data-toggle="tab">Themes</a></li>
                            <li><a href="#FollowingBlogs" data-toggle="tab">Sections</a></li>
                            <li><a href="#FollowingWriters" data-toggle="tab">Writers</a></li>
                        </ul>
                        <div class="tab-content">
                        <div class="tab-pane fade in active" id="FollowingCodes">
                            <div class="portlet-body">
                                <ul class="tabular-data-list">
                                    <li><a href="javascript:;"> <span class="country upper">Country News</span> <span><button type="button" class="like forceLogin" aria-label="Like"> <span aria-hidden="true"><i class="fa fa-star"></i></span></button></span></a></li>
                                    <li><a href="javascript:;"> <span class="cropping upper">Cropping</span> <span><button type="button" class="like forceLogin" aria-label="Like"> <span aria-hidden="true"><i class="fa fa-star"></i></span></button></span></a></li>
                                    <li><a href="javascript:;"> <span class="dairy upper">Dairy</span> <span><button type="button" class="like forceLogin" aria-label="Like"> <span aria-hidden="true"><i class="fa fa-star"></i></span></button></span></a></li>
                                    <li><a href="javascript:;"> <span class="equine upper">Equine</span> <span><button type="button" class="like forceLogin" aria-label="Like"> <span aria-hidden="true"><i class="fa fa-star"></i></span></button></span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane" id="FollowingClubs">
                            <div class="portlet-body">
                                <ul class="tabular-data-list">
                                    <li><a href="javascript:;"> <span>Shepparton</span> <span>
                                        <button type="button" class="like forceLogin" aria-label="Like"> <span aria-hidden="true"><i class="fa fa-star"></i></span> </button>
                                    </span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade fade" id="FollowingBlogs">
                            <div class="portlet-body">
                                <ul class="tabular-data-list">
                                    <li><a href="javascript:;"> <span> <span class="fs16">Dairy</span> <span>dairy.fansunite.com.au</span> </span> <span>
                                        <button type="button" class="like forceLogin" aria-label="Like"> <span aria-hidden="true"><i class="fa fa-star"></i></span> </button>
                                    </span></a></li>
                                    <li><a href="javascript:;"> <span> <span class="fs16">rohit Blog</span> <span>rohit.fansunite.com.au</span> </span> <span>
                                        <button type="button" class="like forceLogin" aria-label="Like"> <span aria-hidden="true"><i class="fa fa-star"></i></span> </button>
                                   </span></a></li>
                                </ul>
                            </div>
                        </div>

                            <div class="tab-pane fade" id="FollowingWriters">
                                <div class="portlet-body">
                                    <ul class="tabular-data-list">
                                        <li><a href="/profile/Cathywalker">
                                            <button type="button" class="like forceLogin" aria-label="Like"> <span aria-hidden="true"><i class="fa fa-star"></i></span> </button>
                                            <div class="img-thumbnail"> <img class="img-responsive" src="https://fu-uploads-uat.s3.amazonaws.com/2015/Nov/12/thumb_FS88FjZlODZGiXgBJgsn.png" alt="" > </div>
                                            <div class="description">
                                                <h4>Cathy Walker</h4>
                                                <p>View Profile</p>
                                            </div>
                                        </a></li>
                                        <li><a href="/profile/Cathywalker">
                                            <button type="button" class="like forceLogin" aria-label="Like"> <span aria-hidden="true"><i class="fa fa-star"></i></span> </button>
                                            <div class="img-thumbnail"> <img class="img-responsive" src="https://fu-uploads-uat.s3.amazonaws.com/2015/Nov/12/thumb_FS88FjZlODZGiXgBJgsn.png" alt="" > </div>
                                            <div class="description">
                                                <h4>Emma Carinci</h4>
                                                <p>View Profile</p>
                                            </div>
                                        </a></li>
                                        <li><a href="/profile/Cathywalker">
                                            <button type="button" class="like forceLogin" aria-label="Like"> <span aria-hidden="true"><i class="fa fa-star"></i></span> </button>
                                            <div class="img-thumbnail"> <img class="img-responsive" src="https://fu-uploads-uat.s3.amazonaws.com/2015/Nov/12/thumb_FS88FjZlODZGiXgBJgsn.png" alt="" > </div>
                                            <div class="description">
                                                <h4>Cathy Walker</h4>
                                                <p>View Profile</p>
                                            </div>
                                        </a></li>
                                        <li><a href="/profile/Cathywalker">
                                            <button type="button" class="like forceLogin" aria-label="Like"> <span aria-hidden="true"><i class="fa fa-star"></i></span> </button>
                                            <div class="img-thumbnail"> <img class="img-responsive" src="https://fu-uploads-uat.s3.amazonaws.com/2015/Nov/12/thumb_FS88FjZlODZGiXgBJgsn.png" alt="" > </div>
                                            <div class="description">
                                                <h4>Emma Carinci</h4>
                                                <p>View Profile</p>
                                            </div>
                                        </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //End Following -->

                <!-- Begin Followed By -->
                <div class="daily-ten clearfix">
                    <header class="sub-header">Followed by</header>
                    <p class="no-data">No follower(s) found.</p>
                </div>
            <!-- //End Followed By -->
            </div>
            <!-- //End User Profile -->
        </div>
    </section>
</div>
<!-- //End Container -->

<?php require "partials/_footer.php"; ?>

<script>
jQuery("#owl-thumbnails").owlCarousel({
	items     				: 2,
	itemsDesktop    		: [1199,2],
	itemsDesktopSmall   	: [980,2],
	itemsTablet    		    : [768,2],
	itemsMobile    		    : [600,1],
	pagination    			: true,
	navigation    			: true,
	loop     				: true,
	autoplay    			: true,
	autoplayTimeout  		:1000,
	navigationText   		: [
		"<i class='fa fa-angle-left fa-2x'></i>",
		"<i class='fa fa-angle-right fa-2x'></i>"
	]
});
</script>
