<?php require "partials/_header.php"; ?>

<!-- Begin Container -->
<div class="page_section">
    <section class="container">
        <!-- Begin Terms -->
        <div class="content-wrap terms_page">
            <h3 class="sub-heading">Terms And Conditions</h3>
            <div class="row">
                <div class="col-full">
                    <div class="content_area">
                        <h1>Terms and conditions</h1>
                        <h2>Terms and conditions</h2>
                        <h3>Terms and conditions</h3>
                        <h4>Terms and conditions</h4>
                        <h5>Terms and conditions</h5>
                        <p>Terms and conditions of <a href="http://www.Fansunite.com.auand"><strong>www.Fansunite.com.auand</strong></a> all relevant subdomains</p>

                        <p>1 Your acceptance of the Terms and Conditions</p>

                        <ul class="la">
                            <li>Welcome to the Fans Unite website (&ldquo;<strong>Website</strong>&rdquo;). The Website is owned and operated by Citizen Media Group (&ldquo;<strong>the Company</strong>&rdquo;, &ldquo;<strong>we</strong>&rdquo; or &ldquo;<strong>us</strong>&rdquo;).</li>
                            <li>The following terms form the Terms and Conditions for use of the Website and the terms of your relationship with us. These Terms and Conditions apply to your use of the Website.</li>
                            <li>Please carefully read these Terms and Conditions before you use the Website. In using the Website, you agree to be bound by these Terms and Conditions, and any other company policies.</li>
                            <li>If you do not accept these Terms and Conditions and agree to comply with the privacy policy and all other policies, you must not continue to use the Website.</li>
                            <li>These Terms and Conditions and the privacy policy and all other policies apply whenever you access the Website, regardless of how you access the Website, including but not limited to access by way of the internet, tablets, smart phones, mobile phones and RSS feeds.</li>
                            <li>We may revise these Terms and Conditions from time to time by updating this document. The revised Terms and Conditions will take effect when they are posted.</li>
                        </ul>

                        <p>2 Membership requirements</p>

                        <p>(a) You will be required to register with the Website in order to gain access to the Website, including access to contribute comments, content or interact with the Website.</p>

                        <p>(b) Registration is free and you agree and acknowledge that you:</p>

                        <ul class="ur">
                            <li>shall provide us with accurate, complete and updated registration information;</li>
                            <li>must safeguard any user name and password which we provide to you;</li>
                            <li>may cancel your registration at any time by notifying us via email at <a href="mailto:Info@fansunite.com.au">Info@fansunite.com.au</a> and</li>
                            <li>consent to our collecting and storing your IP address for security purposes.</li>
                        </ul>

                        <p>(c) We reserve the right to discontinue or cancel your registration, at our sole discretion and without notice for any reason inlcuding, but not limited to, the following:</p>

                        <p>(i) failure to access the Website for an extended period of time;</p>

                        <p>(ii) breach of any applicable law or breach of any of the Terms and Conditions;</p>

                        <p>(iii) where we conclude that your conduct impacts on our name or reputation;</p>

                        <p>(iv) where your registration information is inaccurate, misleading, false or otherwise harmful to us or any third party in any way; or</p>

                        <p>(v) where we conclude that your conduct violates our rights or those of another party.</p>

                        <p>(vi) Where your conduct is in direct conflict or competition with the company</p>

                        <p>(d) We retain the right to contact you on the email address you provide at our discretion. We will never provide your email to third-parties without your consent, except as required by law.</p>

                        <p>(e) To access certain features of the Website, you may be required to hold an existing and valid Twitter and/or Facebook account (&ldquo;<strong>Social Media Account</strong>&rdquo;). By logging into the Website using a Social Media Account, you confirm that:</p>

                        <ul class="styled-check">
                            <li>your Social Media Account is personal to you and is not used by anyone else unless you have authorised them to do so;</li>
                            <li>you comply with these Terms and Conditions and the terms and conditions of use prescribed by Facebook and Twitter;</li>
                            <li>you permit us to view and access certain information from your Social Media Account. You may control the amount of information that is accessible to us by adjusting your Social Media Account &lsquo;s privacy settings. By using the Website, you are authorizing us to collect, store, and retain any and all information that you permit Facebook or Twitter to provide to us;</li>
                            <li>you will not maliciously create additional Social Media Accounts for the purpose of abusing the functionality of the Website or other users, nor do you seek to pass yourself off as another user;</li>
                            <li>if required, you will comply with reasonable requests for further information or verification of your identity; and</li>
                            <li>you consent to our collecting and storing your IP address for security purposes.</li>
                        </ul>

                        <p>3 User Content</p>

                        <ul class="styled-circle">
                            <li>As part of the Website, we may provide interactive communication services such as message boards, chat rooms, blogs, upload facilities, profile pages and other message and communication facilities that provide you with the ability to submit, display and exchange information, messages and other content with us and with other users of the Website.</li>
                            <li>Any content, comment, opinion, view or other material contributed by you, or another user of the Website, is &ldquo;<strong>User Content</strong>&rdquo; and may also include comments, opinions, articles, images and other media.</li>
                            <li>In exchange for allowing you to post and upload User Content, you grant us a non-exclusive, royalty-free, perpetual, worldwide licence to reproduce, publish, host, store, copy modify, adapt, edit, translate, sub-license and otherwise deal with User Content you submit to us by any means whatsoever (including, without limitation, in print and electronic format).</li>
                            <li>You will not post, communicate, transmit or download any User Content on, to and from the Website that:</li>
                        </ul>

                        <ul class="styled-disc">
                            <li>is unoriginal, or in which you do not own the copyright, or which in any way violates or infringes (or could reasonably be expected to violate or infringe) the copyright, registered trademark or intellectual property rights of another person, club, or organisation (including the AFL) or infringes any law;</li>
                            <li>is, or could reasonably be expected to be, obscene, offensive, threatening, abusive, libellous, contemptuous, pornographic, vulgar, profane, indecent or unlawful;</li>
                            <li>is, or could reasonably be expected to be, defamatory of any person;</li>
                            <li>racially or religiously vilifies, incites violence or hatred, or is likely to offend, insult or humiliate others based on race, religion, ethnicity, gender, age, sexual orientation or any physical or mental disability;</li>
                            <li>includes any personal or sensitive information about another person;</li>
                            <li>you know or suspect, or ought reasonably to have known or suspected, to be false, misleading or deceptive;</li>
                            <li>contains a survey, contest, pyramid scheme or improper question;</li>
                            <li>contains content that is advertising, or is promoting or soliciting any goods or services, or otherwise engaging in trade or commerce; without the express consent of Citizen Media Group.</li>
                            <li>is considered a commercial, unsolicited or bulk electronic message which would constitute an infringement of the <em>Spam Act 2003 </em>(Cth);</li>
                            <li>restricts or inhibits any other user from using or enjoying the Website;</li>
                            <li>encourages conduct that would constitute a criminal offence, give rise to civil liability or otherwise violate any law;</li>
                            <li>contains viruses, worms, &ldquo;Trojan horses&rdquo; or any other harmful, contaminating or destructive features; or</li>
                            <li>affects the functionality or operation of the Website or its servers or the functionality or operation of any users&#39; computer systems (for example, by transmitting a computer virus or other harmful component, whether or not knowingly).</li>
                        </ul>

                        <p>(e) By submitting User Content to the Website, you hereby grant to the Company, its assigns and authorised agents, a non-exclusive, royalty-free, perpetual, worldwide licence to reproduce, publish, host, store, copy modify, adapt, edit, translate, sub-license and otherwise deal with the User Content you submit to us by any means and through any media whatsoever.</p>

                        <p>(f) You represent and warrant that you are the creator of all User Content that you submit to the Website and such User Content is true, accurate and not confidential to or is not a trade secret or owned by any other person.</p>

                        <p>(g) You represent and warrant that you have (and will continue to have during your use of the Website) all necessary licenses, rights, consents, and permissions which are required to enable us to use the User Content uploaded by you.</p>

                        <p>(h) To the fullest extent of the law, you waive any moral rights in the User Content that you post or attempt to post to the Website for the purposes of its submission to and publication on the Website.</p>

                        <p>(i) You acknowledge that all communications are public and not private communications and you are solely responsible for any User Content posted by you.</p>

                        <p>4 Website Use and Conduct</p>

                        <p>(a) You must not and you must not permit another person to, without our prior written permission, exploit the Website or content created by the Company and posted on the Website (&ldquo;<strong>Website Content</strong>&rdquo;) for any other purpose or by any other means including, without limitation:</p>

                        <p>(i) store, publish, distribute, communicate to the public, adapt, reverse engineer or decompile any of the Website Content;</p>

                        <p>(ii) create HTTP links from the Website to any other website on the internet, or frame or mirror the Website;</p>

                        <p>(iii) circumvent, disable or otherwise interfere with security-related features of the Website or features that prevent or restrict use or copying of the Website Content;</p>

                        <p>(iv) delete, circumvent or alter any author attribution, legal notices, rights management information or technological protection measures;</p>

                        <p>(v) tamper with, hinder the operation of, or make unauthorised modifications to the Website or Website Content;</p>

                        <p>(vi) transmit any virus, worm, Trojan horse or other disabling feature to or via the Website; or</p>

                        <p>(vii) interfere with the computer systems which support the Website, overload a service, engage in a denial-of-service attack, or attempt to disable a host.</p>

                        <p>(b) You agree that you will not and you will not permit another person to:</p>

                        <p>(viii) use the Website, or any facilities available on the Website, to defame, stalk, harass, threaten, menace or offend any person;</p>

                        <p>(ix) use another person&rsquo;s Social Media Account to access any content on the Website;</p>

                        <p>(x) access or attempt to access information resources you are not authorized to use;</p>

                        <p>(xi) collect or store personal data about other users of the Website; or</p>

                        <p>(xii) impersonate or falsely represent your association with any person or organisation.</p>

                        <p>(c) <strong>Monitoring and Removal of User and User Content</strong></p>

                        <p>We reserve the right and discretion, but not the obligation, to monitor, edit, delete, reject or remove any User Content which you post or seek to post on the Website that we consider infringes these Terms and Conditions.</p>

                        <p>If, in its absolute discretion we determine that you have breached this Clause 3 your access to all or part of the Website may be suspended or terminated with or without notice to you and you may be refused any future use of or access to the Website. Furthermore, any breach which involves illegal activity may be referred to the appropriate law enforcement agency.</p>

                        <p>(d) <strong>Sharing Website Content</strong></p>

                        <p style="margin-left:3.0cm;">We may invite you, by providing a share button on a relevant page, section or part of the Website, to share selected content from the Website through one or more avenues including, but not limited to, email and social media (including but not limited to Facebook and Twitter). If you elect to share Website content through one or more of those avenues, you must do so by making use of the share button provided by us. You agree and acknowledge that you share any content at your own risk.</p>

                        <p>(e) <strong>Linking to the Website</strong></p>

                        <p style="margin-left:3.0cm;">You may only link to the Website, other than by making use of the sharing features on the Website, with the prior written consent of the Company. Any links, if allowed, must link directly to the Website home page unless otherwise agreed in writing by the Company. To request permission to link to this site, please email: Tom@fansunite.com.au.</p>

                        <p>5 Website Intellectual Property</p>

                        <p>(a) You acknowledge that the material included in the Website is the subject of copyright and that it is the subject of other intellectual property and legal rights.</p>

                        <p>(b) Unless expressly stated otherwise, all intellectual property other than User Content in relation to the Website and the content on the Website (including the software, design, text, data, icons, logos, copyrights, designs, trade marks, concepts, sound recordings and graphics comprised in the Website) (&ldquo;<strong>Intellectual Property</strong>&rdquo;) belongs to The Company or its licensors, advertisers, associated or related entites or affiliates.</p>

                        <p>(c) The Company retains all right, title and interest in and to the Website and all related content developed for the Website. Nothing you do on or in relation to the Website, or any of the related content will transfer any intellectual property rights to you, or license to you any intellectual property rights in and to the Website, unless expressly stated.</p>

                        <p>(d) Except where otherwise stated, all registered trade marks used on this Website have been used with the permission of the relevant trade mark owner. You must not use any trade marks without the prior, specific, written permission of its owner.</p>

                        <p>(e) You agree not to do anything which interferes with or breaches the intellectual property rights in the content. You agree not to copy, modify, create a derivative work, reverse engineer, reverse assemble, attempt to discover the source code, sell, assign, sub-license, grant a security interest in or otherwise transfer any content on the Website.</p>

                        <p>(f) You may download and view content or print a copy of material on the Website for personal, non-commercial use, provided that you do not modify the content in any way (including any copyright notice).</p>

                        <p>(g) You may reprint or electronically reproduce Website Content for personal non-commercial use provided that you do not modify the Website Content in any way (including any copyright notice).</p>

                        <p>(h) Except as permitted by these Terms and Conditions, permission to reprint or electronically reproduce content from the Website, or any related content in whole or in part for any other purpose is expressly prohibited, unless prior written consent is obtained from us. You may contact us via the communciation methods available on the Website if you wish to obtain such consent.</p>

                        <p>(i) We respect the intellectual property rights of others. If you believe that content appearing on the Website violates your rights, or you have not been properly credited or there has been an oversight by us,, please contact Tom@Fansunite.com.au</p>

                        <p>6 Third party content and links</p>

                        <p>(a) This Website may contain links to third party websites including Facebook and Twitter. These third party websites are not under the control of the Company and we are not responsible or liable for the contents, accuracy, legality, suitability or reliability that third party website or any hyperlink contained in that third party website.</p>

                        <p>(b) When you access a third party website, you do so at your own risk and acknowledge that your use of the third party websites may be governed by separate terms and conditions and privacy policies.</p>

                        <p>(c) The Company accepts no liability for any loss incurred by you directly or indirectly as a result of your reliance on information on third party website and accepts no liability and disclaims all warranties, express or implied, regarding the use of goods or services provided through any third party website.</p>

                        <p>7 Disclaimer</p>

                        <p>(a) The Company, its affiliates or any of their respective directors, officers, employees or agents makes no representation or warranty as to the accuracy, legality, suitability or reliability of the information contained as part of the Website Content and none of them accept any responsibility arising in any way (including negligently) for errors in, or omissions from, the information contained as part of the Website Content or your reliance on such information.</p>

                        <p>(b) You acknowledge that the opinions expressed on this site are those of the relevant contributors and may not necessarily represent those views of the Company or those of our employees or representatives. </p>

                        <p>(c) You agree and acknowledge that the Company is not responsible for any interpretation, opinion or conclusion you form from the information provided on the Website. Any reliance upon any content, opinion, representation or statement contained as part of the Website Content is at your sole risk.</p>

                        <p>(d) The Company is not associated or affiliated in any way with any sporting body, club or organisation including the AFL or any of the AFL clubs. All trademarks, logos, and images are the property of their respective and rightful owners.</p>

                        <p>8 No Liability</p>

                        <p>(a) You release us from any liability arising as a result of your reliance on any content contributed by another user on the Website.</p>

                        <p>(b) In no event will the Company, its related bodies corporate and any of their respective directors, officers, employees or agents be liable in contract, tort (including negligence) or otherwise, for any direct, indirect, special, consequential or punitive loss, personal injury or damages or any loss or damages whatsoever, arising out of or in connection with:</p>

                        <p>(i) the use or access or any ability to use or access the Website or its contents;</p>

                        <p>(ii) the information on the Website being inaccurate, incomplete or misleading;</p>

                        <p>(iii) a failure or omission on the part of the Company to comply with its obligations under these Terms and Conditions.</p>

                        <p>(c) The Company is not liable to you or anyone else if interference with or damage to your computer systems occurs in connection with use of the Website or any third party website. You must take your own precautions to ensure that whatever you select for your use from the Website or any third party website free of viruses or any other thing that may interfere with or damage the operations of your computer systems.</p>

                        <p>9 No Warranties</p>

                        <p>(a) The Company does not warrant that the Website, or functions contained on the Website will be uninterrupted or error free, or that defects will be corrected or that the Website or the hosting server are free of viruses or bugs.</p>

                        <p>(b) The Company is not responsible for any problems or technical malfunctions of any telephone network, computer online systems, servers or providers, computer equipment, software, technical problems or traffic congestion on the internet or at any website or any combination thereof.</p>

                        <p>(c) You acknowledge that use of the Website is at your sole risk.</p>

                        <p>10 Indemnity</p>

                        <p>(a) You agree to fully indemnify and hold harmless the Company and its related entities, including its officers, directors, employees, agents, subcontractors, and licensors from and against any and all claims, actions, demands, liabilities, payments, settlements, costs, loss (including consequential loss) or damage including legal and accounting fees arising in connection with:</p>

                        <p>(i) your activities on or use of the Website , or resulting from or alleged to result from, your activities on or use of the Website</p>

                        <p>(ii) any misrepresentation, failure, negligence by you and/or as a result of any breach by you of these Terms and Conditions, in particular, Clause 3 and Clause 4;</p>

                        <p>(iii) a breach of your representations and warranties under these Terms and Conditions;</p>

                        <p>(iv) a violation by you of any of these Terms and Conditions;</p>

                        <p>(v) any dealings or complaints made against you by other parties.</p>

                        <p>11 General</p>

                        <p>(a) These Terms and Conditions shall be construed and applied in accordance with the laws of Victoria, Australia. You consent to the exclusive jurisdiction of the Victorian Courts, to determine any matter or dispute which arises under these terms and conditions in accordance with the laws of Victoria, Australia.</p>

                        <p>(b) These Terms and Conditions and the policies referred to herein constitute the entire agreement between us and you in relation to the Website and your use of the Website supersedes all other (prior or contemporaneous) communications or displays whether electronic, oral, or written, between us and you in relation to the Website.</p>

                        <p>(c) The provisions of these Terms and Conditions which by their nature survive termination or expiry of these Terms and Conditions, will survive cancellation of your registration on the Website.</p>

                        <p>(d) If any of these Terms and Conditions are invalid, unenforceable or illegal, that term will be struck out and the remaining terms will remain in force.</p>

                        <p>(e) If we do not take any steps in relation to a particular breach by you of these Terms and Conditions, this will not be treated as a waiver by us of our right to act with respect to that breach, and in respect of subsequent or similar breaches.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- //End Terms -->
    </section>
</div>
<!-- //End Container -->

<?php require "partials/_footer.php"; ?>

<script>
jQuery("#owl-thumbnails").owlCarousel({
	items     				: 2,
	itemsDesktop    		: [1199,2],
	itemsDesktopSmall   	: [980,2],
	itemsTablet    		    : [768,2],
	itemsMobile    		    : [600,1],
	pagination    			: true,
	navigation    			: true,
	loop     				: true,
	autoplay    			: true,
	autoplayTimeout  		:1000,
	navigationText   		: [
		"<i class='fa fa-angle-left fa-2x'></i>",
		"<i class='fa fa-angle-right fa-2x'></i>"
	]
});
</script>
