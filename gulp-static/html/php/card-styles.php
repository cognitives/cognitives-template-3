<?php require("partials/_header.php") ?>

<?php require("partials/cards/_banner-01.php") ?>
<?php require("partials/cards/_banner-02.php") ?>
<?php require("partials/cards/_banner-03.php") ?>
<?php require("partials/cards/_banner-04.php") ?>
<?php require("partials/cards/_banner-05.php") ?>

<div class="container">
  <div class="row">
    <div class="col-two-thirds"><?php require("partials/cards/_technology-01.php") ?></div>
    <div class="col-two-thirds"><?php require("partials/cards/_technology-02.php") ?></div>
    <div class="col-two-thirds"><?php require("partials/cards/_twitter-04.php") ?></div>
    <div class="col-two-thirds"><?php require("partials/cards/_twitter-05.php") ?></div>
    <div class="col-two-thirds"><?php require("partials/cards/_facebook-03.php") ?></div>
    <div class="col-two-thirds"><?php require("partials/cards/_youtube-02.php") ?></div>
    <div class="col-two-thirds"><?php require("partials/cards/_instagram-03.php") ?></div>
    <div class="col-two-thirds"><?php require("partials/cards/_featured-writer-01.php") ?></div>
    <div class="col-two-thirds"><?php require("partials/cards/_featured-writers-01.php") ?></div>
  </div>
  <div class="row">
    <div class="col-half">
      <?php require("partials/cards/_technology-01.php") ?>
      <?php require("partials/cards/_technology-02.php") ?>
      <?php require("partials/cards/_facebook-03.php") ?>
    </div>
    <div class="col-half">
      <?php require("partials/cards/_twitter-04.php") ?>
      <?php require("partials/cards/_twitter-05.php") ?>
      <?php require("partials/cards/_instagram-03.php") ?>
    </div>
  </div>
  <div class="row">
    <div class="col-third">
      <?php require("partials/cards/_technology-01.php") ?>
      <?php require("partials/cards/_technology-02.php") ?>
      <?php require("partials/cards/_facebook-03.php") ?>
    </div>
    <div class="col-third">
      <?php require("partials/cards/_twitter-04.php") ?>
      <?php require("partials/cards/_twitter-05.php") ?>
      <?php require("partials/cards/_instagram-03.php") ?>
    </div>
    <div class="col-third">
      <?php require("partials/cards/_youtube-02.php") ?>
      <?php require("partials/cards/_featured-writer-01.php") ?>
      <?php require("partials/cards/_featured-writers-01.php") ?>
    </div>
  </div>
  <div class="row">
    <div class="col-half">
      <?php require("partials/cards/_facebook-01.php") ?>
      <?php require("partials/cards/_facebook-02.php") ?>
      <?php require("partials/cards/_media-01.php") ?>
      <?php require("partials/cards/_twitter-01.php") ?>
      <?php require("partials/cards/_youtube-01.php") ?>
    </div>
    <div class="col-half">
      <?php require("partials/cards/_instagram-01.php") ?>
      <?php require("partials/cards/_instagram-02.php") ?>
      <?php require("partials/cards/_media-02.php") ?>
      <?php require("partials/cards/_twitter-02.php") ?>
      <?php require("partials/cards/_youtube-03.php") ?>
    </div>
  </div>

  <div class="row">
    <div class="col-quarter"><?php require("partials/cards/_technology-01.php") ?></div>
    <div class="col-quarter"><?php require("partials/cards/_technology-02.php") ?></div>
    <div class="col-quarter"><?php require("partials/cards/_twitter-04.php") ?></div>
    <div class="col-quarter"><?php require("partials/cards/_twitter-05.php") ?></div>
  </div>
  <div class="row">
    <div class="col-quarter"><?php require("partials/cards/_youtube-02.php") ?></div>
    <div class="col-quarter"><?php require("partials/cards/_instagram-03.php") ?></div>
    <div class="col-quarter"><?php require("partials/cards/_featured-writer-01.php") ?></div>
    <div class="col-quarter"><?php require("partials/cards/_featured-writers-01.php") ?></div>
  </div>
  <div class="row">
    <div class="col-quarter"><?php require("partials/cards/_facebook-01.php") ?></div>
    <div class="col-quarter"><?php require("partials/cards/_facebook-02.php") ?></div>
    <div class="col-quarter"><?php require("partials/cards/_media-01.php") ?></div>
    <div class="col-quarter"><?php require("partials/cards/_twitter-01.php") ?></div>
  </div>
  <div class="row">
    <div class="col-quarter"><?php require("partials/cards/_youtube-01.php") ?></div>
    <div class="col-quarter"><?php require("partials/cards/_instagram-01.php") ?></div>
    <div class="col-quarter"><?php require("partials/cards/_instagram-02.php") ?></div>
    <div class="col-quarter"><?php require("partials/cards/_media-02.php") ?></div>
  </div>

  <div class="row">
    <div class="col-third-short">
      <?php require("partials/cards/_marketing-01.php") ?>
      <?php require("partials/cards/_instagram-02.php") ?>
      <?php require("partials/cards/_digital-01.php") ?>
      <?php require("partials/cards/_media-05.php") ?>
    </div>
    <div class="col-third-short">
      <?php require("partials/cards/_facebook-01.php") ?>
      <?php require("partials/cards/_facebook-02.php") ?>
      <?php require("partials/cards/_media-01.php") ?>
      <?php require("partials/cards/_twitter-01.php") ?>
      <?php require("partials/cards/_youtube-01.php") ?>
    </div>
    <div class="col-third-short">
      <?php require("partials/cards/_instagram-01.php") ?>
      <?php require("partials/cards/_instagram-02.php") ?>
      <?php require("partials/cards/_media-02.php") ?>
      <?php require("partials/cards/_twitter-02.php") ?>
      <?php require("partials/cards/_youtube-03.php") ?>
    </div>
  </div>
</div>


<?php require("partials/_footer.php") ?>