<?php require "partials/_header.php"; ?>

<!-- Begin User Profile Page -->
<div class="user_profile page_section">
    <div class="container">
        <h3 class="sub-heading">Account &amp; Profile</h3>
        <div class="user_profile_section">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="profile-status text-center">
                        <p>Your profile is <span class="grey_themer">90%</span> complete</p>
                    </div>
                    <div class="profile-upload text-center ">
                        <div class="user-pic"><span><i class="fa fa-user"></i></span></div>
                        <a href="partials/_profile-pic-upload.php" data-toggle="modal" data-target="#AjaxUserProfileModal" data-remote="false" class="em grey_themer upload-profile"><i class="fa fa-plus-circle green"></i>add new image</a>
                    </div>

                    <div class="profile-details">
                        <form id="profile-form" class="form-horizontal" action="#" method="post" enctype="multipart/form-data">
                            <div class="controls-full field-user-username required">
                                <label for="user-username">Username</label>
                                <input type="text" id="user-username" class="form-control" name="User[username]" value="admin" placeholder="Fans Unite username" data-required="1">
                                <span class='help-block'>Update username – You can only change this once</span>
                            </div>

                            <div class="controls-full field-user-firstname required">
                                <label for="user-firstname">First Name</label>
                                <input type="text" id="user-firstname" class="form-control required" name="User[firstname]" value="Rohit" placeholder="First name" data-required="1">
                                <span class='help-block'>Update your first name</span>
                            </div>

                            <div class="controls-full field-user-lastname required">
                                <label for="user-lastname">Last Name</label>
                                <input type="text" id="user-lastname" class="form-control required" name="User[lastname]" value="Gupta" placeholder="Last name" data-required="1" disabled>
                                <span class='help-block'>Update your last name</span>
                            </div>

                            <div class="controls-full field-user-by_line">
                                <label for="user-by_line">Byline</label>
                                <div class="button-set grey">
                                    <div class="column">
                                        <label><input type="radio" id="User_by_line0" name="User[by_line]" value="FirstNameLastName" checked='checked'><span><!-- fake checkbox --></span>First Name Last Name</label>
                                    </div>

                                    <div class="column">
                                        <label><input type="radio" id="User_by_line1" name="User[by_line]" value="Username"><span><!-- fake checkbox --></span>Username</label>
                                    </div>
                                </div>
                                <div class="help-block"></div>
                            </div>

                            <div class="controls-full field-user-bio">
                                <label for="user-bio">Bio</label>
                                <textarea id="user-bio" class="form-control" name="User[bio]" rows="3" placeholder="Write your description..."></textarea>
                                <span class='help-block'>Tell us a bit about yourself</span>
                            </div>

                            <div class="controls-full field-user-email required">
                                <label for="user-email">Email</label>
                                <input type="text" id="user-email" class="form-control required" name="User[email]" value="rohitg@ideoris.com.au" placeholder="Email" data-required="1">
                                <span class='help-block'>Update your email address</span>
                            </div>

                            <div class="controls-full field-user-password">
                                <label for="user-password">Password</label>
                                <input type="password" id="user-password" class="form-control required" name="User[password]" placeholder="Leave empty to not update" data-required="1">
                                <span class='help-block'>Change your password</span>
                            </div>

                            <div class="controls-full field-user-verifypassword">
                                <label for="user-verifypassword">Verify Password</label>
                                <input type="password" id="user-verifypassword" class="form-control required" name="User[verifypassword]" placeholder="Leave empty to not update" data-required="1">
                                <span class='help-block'>Type your password again</span>
                            </div>

                            <div class="button-full">
                                <button type="submit" class="button grey button-large">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //End User Profile Page -->


<!-- Begin Ajax based My Platform Modal Popup -->
<div class="modal fade" id="AjaxMyPlatformModal" tabindex="-1" role="dialog" aria-labelledby="AjaxMyPlatformModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<button type="button" class="modal-close x-close" data-dismiss="modal"><span class="sr-only">Close</span></button>

			<div class="modal-body nopadd"></div>
		</div>
	</div>
</div>
<!-- //End Ajax based Profile Modal Popup -->

<!-- Begin Ajax based My Platform Modal Popup -->
<div class="modal fade" id="AjaxUserProfileModal" tabindex="-1" role="dialog" aria-labelledby="AjaxUserProfileModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<button type="button" class="modal-close x-close" data-dismiss="modal"><span class="sr-only">Close</span></button>
            <div class="modal-header">
                <h3>Add a profile pic</h3>
            </div>
			<div class="modal-body nopadd"></div>
		</div>
	</div>
</div>
<!-- //End Ajax based Profile Modal Popup -->


<?php require "partials/_footer.php"; ?>
