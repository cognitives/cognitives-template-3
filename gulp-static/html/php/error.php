<?php require("partials/_header.php") ?>

<div class="page_section">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-sm-10 col-xs-12 col-centered">
                <div class="error-wrapper text-center">
                    <img src="images/icons/error.svg" class="error-icon" alt="Error Icon" />
                    <div class="error-message">
                        <h1 class="error-heading">403 <span>Forbidden</span></h1>
                        <p class="content-section"><span>Whoa there! Sorry,</span> but this blog doesn’t exist</p>
                        <a class="button red home-button"><i class="fa fa-long-arrow-left"></i>Back to Homepage</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require("partials/_footer.php") ?>
