<div class="responsive-navigation" role="navigation">
    <div class="close-menu" style="display: block;"><svg class="icon_close" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xml:space="preserve" viewBox="0 0 512 512" version="1.1" enable-background="new 0 0 512 512">
    <path d="M284.1,256L506.2,33.9c7.8-7.8,7.8-20.3,0-28.1s-20.3-7.8-28.1,0L256,227.9L33.9,5.8 c-7.8-7.8-20.3-7.8-28.1,0s-7.8,20.3,0,28.1L227.9,256L5.8,478.1c-7.8,7.8-7.8,20.3,0,28.1c3.9,3.9,9,5.8,14,5.8s10.2-1.9,14-5.8 L256,284.1l222.1,222.1c3.9,3.9,9,5.8,14,5.8s10.2-1.9,14-5.8c7.8-7.8,7.8-20.3,0-28.1L284.1,256z">
    </svg></div>
    <ul class="responsive-navigation__list">
        <li class="responsive-navigation__item"><a href="blog.php" class="responsive-navigation__link">Blog</a></li>
        <li class="responsive-navigation__item"><a href="article.php" class="responsive-navigation__link">Article</a>
            <ul class="sub-menu">
                <li><a href="#">Lorem Ipsum</a></li>
                <li><a href="#">Lorem Ipsum Dolor</a></li>
                <li><a href="#">Lorem Ipsum Sit</a></li>
                <li><a href="#">Lorem Ipsum Amet</a></li>
            </ul>
        </li>
        <li class="responsive-navigation__item"><a href="card-styles.php" class="responsive-navigation__link">Card Styles</a>
            <ul class="sub-menu">
                <li><a href="#">Lorem Ipsum</a></li>
                <li><a href="#">Lorem Ipsum Dolor</a></li>
                <li><a href="#">Lorem Ipsum Sit</a></li>
                <li><a href="#">Lorem Ipsum Amet</a></li>
            </ul>
        </li>
        <li class="responsive-navigation__item"><a href="#" class="responsive-navigation__link">Technology</a></li>
        <li class="responsive-navigation__item"><a href="#" class="responsive-navigation__link">Digital</a></li>
        <li class="responsive-navigation__item"><a href="#" class="responsive-navigation__link">Innovation</a></li>
    </ul>
    </div>
</div>
