<div class="you_may_also_like">
    <div class="container">
        <h1 class="footer__heading">You may also enjoy these...</h1>

        <div class="row">
            <div class="cards_main">
                <div class="col-quarter">
                    <a href="#" class="card card__news withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-quarter">
                    <a href="#" class="card card__news withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-quarter">
                    <a href="#" class="card card__news withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-quarter">
                    <a href="#" class="card card__news withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
