<div class="card card__banner card--creative full_screen" style="background-image:url(images/data/background-banner-01.jpg)">
  <div class="card__content">
    <a href="#" class="card__category">Creative</a>

    <h1 class="card__heading">Is there still a place for <br> pro photographers?</h1>

    <?php require "partials/cards/_caption-02.php"; ?>
  </div>
</div>
