<?php require 'partials/cards/_btn-overlay.php'; ?>
<div class="card-image" style="background-image:url('https://placeholdit.imgix.net/~text?txtsize=75&bg=e8117f&txtclr=ffffff&txt=1000%C3%971000&w=1000&h=1000')">
	<div class="play_icon"></div>
</div>
<div class="content-section">
    <div class="title-section">
        <span>Social</span>
        <div class="card-icon"></div>
    </div>
    <h1 class="heading-section">Title goes here Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam,</h1>
    <p class="description">Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat... Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...</p>
    <div class="caption_bottom">
        <div class="author_name">Social Name</div>
        <div class="post_date">Oct 5 2015</div>
        <span class="category_share_icon social_share_toggler">
            <i class="fa fa-share-alt"></i>
            <div class="tooltip">
                <div class="tooltip__link tooltip__link--facebook"></div>
                <div class="tooltip__link tooltip__link--twitter"></div>
                <div class="tooltip__link tooltip__link--google-plus"></div>
            </div>
        </span>

    </div>
</div>
