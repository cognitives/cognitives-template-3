<a href="#" class="card card--marketing">
  <div class="card__image" style="background-image:url(images/data/background-09.jpg);"></div>
  <div class="card__content">
    <div class="card__category">marketing</div>
    <h1 class="card__heading">Great parks for your daily nature walk</h1>
  </div>
</a>
