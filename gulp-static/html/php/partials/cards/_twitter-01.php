<div class="card card__twitter">
  <div class="card__no-image"></div>
  <div class="card__content">
    <div class="card__category">twitter</div>
    <?php require "partials/cards/_caption-03.php"; ?>
    <p class="card__text">
      You can have all the latest and greatest gear in the world. Doesn’t mean you have an actual clue though.
      <span class="card__tag">#clueless</span>
    </p>
  </div>
</div>
