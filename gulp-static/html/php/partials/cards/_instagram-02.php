<a href="#" class="card card__instagram">
  <div class="card__image" style="background-image:url(images/data/background-11.jpg);"></div>
  <div class="card__content">
    <div class="card__category">instagram</div>
    <div class="card__tag">
      <span class="card__tag-paragraph">#inspiration</span>
      <span class="card__tag-paragraph">#everyday</span>
    </div>
  </div>
</a>
