<div class="card__caption">
  <div class="card__icon-social">
    <div class="tooltip">
      <div class="tooltip__link tooltip__link--facebook"></div>
      <div class="tooltip__link tooltip__link--twitter"></div>
      <div class="tooltip__link tooltip__link--google-plus"></div>
    </div>
  </div>
  <div class="card__avatar" style="background-image:url(images/data/avatar-07.jpg);">
    <div class="card__avatar-follow"></div>
  </div>
  <div class="card__author">Ted Talks Australia</div>
  <div class="card__date">8 OCT 2015</div>
  <div class="card__read-time">12 min read</div>
</div>
