<div class="card card__banner card__twitter" style="background-image:url(images/data/background-17.jpg)">
  <div class="card__content">
    <a href="#" class="card__category">Twitter</a>

    <?php require "partials/cards/_caption-02.php"; ?>

    <h1 class="card__text">
      You can have all the latest gear in the world. <br>
      Doesn't mean you have an actual clue though
      <!-- <span class="card__tag">#clueless</span> -->
    </h1>
  </div>
</div>
