<div class="card card__featured">
  <div class="card__featured-left"  style="background-image:url(images/data/background-02.jpg);">
    <h1 class="card__featured-heading">Popular Writers</h1>
    <div class="card__featured-text">Haven’t joined yet?</div>
    <a href="#" class="button">Join Us</a>
  </div>
  <div class="card__featured-right">
    <div class="card__category">popular writers</div>
    <a href="#" class="card__featured-link">
      <div class="card__avatar" style="background-image:url(images/data/avatar-01.jpg);">
        <div class="card__avatar-follow"></div>
      </div>
      <div class="card__featured-author">andrew wilkinson</div>
      <div class="">20K subscribers</div>
    </a>
    <a href="#" class="card__featured-link">
      <div class="card__avatar" style="background-image:url(images/data/avatar-02.jpg);">
        <div class="card__avatar-follow"></div>
      </div>
      <div class="card__featured-author">drake edwards</div>
      <div class="">15K subscribers</div>
    </a>
    <a href="#" class="card__featured-link">
      <div class="card__avatar" style="background-image:url(images/data/avatar-03.jpg);">
        <div class="card__avatar-follow"></div>
      </div>
      <div class="card__featured-author">nike hardie</div>
      <div class="">11K subscribers</div>
    </a>
  </div>
</div>
