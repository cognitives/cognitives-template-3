<div class="card__caption">
  <div class="card__icon-social">
    <div class="tooltip">
      <div class="tooltip__link tooltip__link--facebook"></div>
      <div class="tooltip__link tooltip__link--twitter"></div>
      <div class="tooltip__link tooltip__link--google-plus"></div>
    </div>
  </div>
  <div class="card__avatar" style="background-image:url(images/data/avatar-05.jpg);">
    <div class="card__avatar-follow"></div>
  </div>
  <div class="card__author">By Sarah Wilkinson</div>
  <div class="card__read-time">12 min read</div>
</div>
