<a href="#" class="card card__facebook card--light">
  <div class="card__image" style="background-image:url(images/data/background-18.jpg);"></div>
  <div class="card__content">
    <div class="card__category">facebook</div>
    <p class="card__text">
      User text nibh veliod del of Lorem Ipsum. Proin gravida nibh veliod del mutte.
      <span class="card__tag">#travel</span>
      <span class="card__tag">#photography</span>
    </p>
    <?php require "partials/cards/_caption-01.php"; ?>
  </div>
</a>
