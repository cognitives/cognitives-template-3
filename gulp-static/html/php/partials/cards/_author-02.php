<div class="author">
  <div class="author__image" style="background-image:url(images/data/background-10.jpg);"></div>
  <div class="author__avatar" style="background-image:url(images/data/avatar-01.jpg);"></div>

  <h1 class="author__name">Chris Barlow</h1>
  <p class="author__title">Thought Leadership</p>

  <a href="#" class="button">FOLLOW</a>

  <div class="author__links">
    <a href="#" class="author__link author__link--followers">
      <strong>487</strong>
      Followers
    </a>
    <a href="#" class="author__link author__link--following">
      <strong>24</strong>
      Following
    </a>
    <a href="#" class="author__link author__link--articles">
      <strong>46</strong>
      Articles
    </a>
  </div>
</div>
