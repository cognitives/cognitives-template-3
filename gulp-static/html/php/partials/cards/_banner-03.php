<div class="card card__banner card__youtube" style="background-image:url(images/data/youtube-01.jpg)">
  <div class="card__content">
    <div class="card__category">youtube</div>
    <div class="card__icon-youtube"></div>
    <h1 class="card__heading">Use Art to Turn the<br>World Inside Out</h1>

    <?php require "partials/cards/_caption-04.php"; ?>
  </div>
</div>
