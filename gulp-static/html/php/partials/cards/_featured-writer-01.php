<div class="card card__featured">
  <div class="card__featured-left" style="background-image:url(images/data/background-03.jpg);">
      <h1 class="card__featured-heading">Jason Andrews</h1>
      <div class="card__featured-title">photographer</div>
      <a href="#" class="card__featured-subscribers">50K Subscribers</a>
  </div>
  <div class="card__featured-right">
    <div class="card__category">popular articles</div>

    <a href="#" class="card__featured-link">
      <div class="card__featured-views">12,000 Views</div>
      <div class="card__featured-sub-heading">7 with VII: All About Gear</div>
      <div class="">READ</div>
    </a>

    <a href="#" class="card__featured-link">
      <div class="card__featured-views">7,400 Views</div>
      <div class="card__featured-sub-heading">It’s the Pictures You Don’t Take</div>
      <div class="">READ</div>
    </a>
  </div>
</div>
