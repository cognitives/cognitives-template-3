<div id="header__menu">
    <div class="profile__container">
        <div class="profile__menu">
            <div class="profile__menu-image" style="background-image:url(images/data/avatar-05.jpg);"></div>
            <div class="profile__menu-content">
                <div class="profile__menu-name">Michelle Love</div>
                <div class="profile__menu-email">mish@gmail.com</div>
                <a href="#" class="button">My Feed</a>
            </div>
            <ul class="profile__menu-navigation">
                <li><a class="profile__menu-navigation-link" href="login.php">Login</a></li>
                <li><a class="profile__menu-navigation-link" href="#">Profile</a></li>
                <li><a class="profile__menu-navigation-link" href="#">Settings</a></li>
                <li><a class="profile__menu-navigation-link" href="#">Sign Out</a></li>
            </ul>
        </div>
    </div>
</div>
