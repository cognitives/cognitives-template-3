<div class="login full_screen">
  <div class="login__content">
    <h1 class="login__heading">Join the Future of Marketing</h1>
    <p class="login__text">This is the place for you to listen and learn, speak and teach,<br> connect and build. Welcome to being connected.</p>
    <div class="login__form">
      <div class="login--arrow"></div>
      <div class="row">
        <div class="form__col">
          <input name="username" class="input__text" type="text" placeholder="Name">
        </div>
        <div class="form__col">
          <input name="email" type="email" class="input__text" placeholder="Email">
        </div>
      </div>
      <button class="button--large">
        <h2 class="">Create an account</h2>
        <p class="">So I can help shape the future of marketing</p>
      </button>
      <button class="button--white">Login</button>
    </div>
  </div>
</div>