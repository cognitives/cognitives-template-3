<div class="slides">
    <div class="slide video">
        <video controls preload="none" poster="images/data/thumbnail-full-article-01.png">
            <source src="images/data/video.mp4" type="video/mp4">
			<source src="images/data/video.webm" type="video/webm">
			<source src="images/data/video.ogg" type="video/ogg">
        </video>
        <div class="slide__content">
            <div class="slide__caption"><p>Image caption</p></div>
        </div>
    </div>
    <div class="slide">
        <div class="slide__content">
            <h1 class="slide__heading">DIGITAL</h1>
            <div class="slide__sub-heading">Want to learn more about the digital space?</div>
            <p class="slide__description">The future of marketing is definitely digital, follow this category and stay<br> up to date with all the latest innovations that are engaing audiences.</p>
            <a href="#" class="button">Follow Digital</a>
            <div class="slide__caption"><p>Image caption</p></div>
        </div>
    </div>
    <div class="slide">
        <div class="slide__content">
            <h1 class="slide__heading">DIGITAL</h1>
            <div class="slide__sub-heading">Want to learn more about the digital space?</div>
            <p class="slide__description">The future of marketing is definitely digital, follow this category and stay<br> up to date with all the latest innovations that are engaing audiences.</p>
            <a href="#" class="button">Follow Digital</a>
            <div class="slide__caption"><p>Image caption</p></div>
        </div>
    </div>
</div>
