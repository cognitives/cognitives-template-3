<div id="upload-page-modal">
    <form action="js/dropzone/upload.php" class="dropzone" id="dropzone"></form>
</div>

<script>
$(document).ready(function() {
	"use strict";
	
	Dropzone.autoDiscover = false;
	var myDropzone = new Dropzone("#dropzone", {
		addRemoveLinks: true,
	})
});
</script>