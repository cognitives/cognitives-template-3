<?php require "partials/_header.php"; ?>

<!-- Begin Container -->
<div class="page_section">
    <section class="container">
        <!-- Begin ContactUs -->
        <div class="content-wrap contact_us_page">
            <div class="row">
                <div class="col-third">
                    <div class="contact_details">
                        <h3 class="sub-heading">Contact us</h3>
                        <p class="has-border-bottom">You can contact us direct by:</p>

                        <ul class="address">
                            <li><span>Email</span> <a class="green" href="mailto:enquiry@fansunite.com.au">Josh@fansunite.com.au</a></li>
                            <li><span>Postal</span> 42 Moray St<br />
                            Southbank Melbourne<br />
                            Victoria 3000</li>
                        </ul>
                        <ul class="social">
                            <li class="facebook"><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li class="twitter"><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li class="g-plus"><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-two-thirds">
                    <h3 class="sub-heading">Leave us a message</h3>
                    <p class="has-border-bottom">Please fill out the form below to contact us.</p>

                    <div class="alert alert-success fade in hide" id="contactMessage">
                        Thank you for contacting us. We will respond to you as soon as possible.
                    </div>

                    <div class="alert alert-danger alert-dismissible fade in hide form-contact" role="alert" id="contactErrorContainer">
                        <h4>Oh snap! You got an error!</h4>
                        <div id="contactErrorContainerContent"></div>
                    </div>

                    <form id="contactForm" class="form-horizontal form-contact" action="#" method="post" autoComplete="off">
                        <div class="alert alert-danger alert-dismissible fade in" style="display:none">
                            <button class="close" aria-label="Close" data-dismiss="alert" type="button"><span aria-hidden="true">×</span></button>

                        </div>
                        <div class="controls-full">
                            <div class="form-group field-contactform-name required">
                                <label class="control-label" for="contactform-name">Name</label>
                                <input type="text" id="contactform-name" class="form-control error" name="ContactForm[name]" placeholder="Name">

                            </div>
                        </div>
                        <div class="controls-full">
                            <div class="form-group field-contactform-email required">
                                <label class="control-label" for="contactform-email">Email</label>
                                <input type="text" id="contactform-email" class="form-control" name="ContactForm[email]" placeholder="Email">

                            </div>
                        </div>
                        <div class="controls-full">
                            <div class="form-group field-contactform-message required">
                                <label class="control-label" for="contactform-message">Message</label>
                                <textarea id="contactform-message" class="form-control" name="ContactForm[message]" placeholder="Message"></textarea>

                            </div>
                        </div>
                        <button type="submit" id="btnContact" class="button grey lg" name="contact-button">Send</button>
                    </form>
                </div>


            </div>
        </div>
        <!-- //End ContactUs -->
    </section>
</div>
<!-- //End Container -->

<?php require "partials/_footer.php"; ?>

<script>
jQuery("#owl-thumbnails").owlCarousel({
	items     				: 2,
	itemsDesktop    		: [1199,2],
	itemsDesktopSmall   	: [980,2],
	itemsTablet    		    : [768,2],
	itemsMobile    		    : [600,1],
	pagination    			: true,
	navigation    			: true,
	loop     				: true,
	autoplay    			: true,
	autoplayTimeout  		:1000,
	navigationText   		: [
		"<i class='fa fa-angle-left fa-2x'></i>",
		"<i class='fa fa-angle-right fa-2x'></i>"
	]
});
</script>
