<?php require("partials/_header-login.php") ?>

<div class="page_section article_page">
    <div class="container">
        <div class="article article--creative">
            <div class="article__banner"><?php require("partials/_slides.php") ?></div>

            <div class="article__header">
                <div class="article__category">Creative Creative Creative</div>
                <div class="article__social">
                    <a href="#" class="article__social-link article__social-link--twitter"></a>
                    <a href="#" class="article__social-link article__social-link--facebook"></a>
                    <a href="#" class="article__social-link article__social-link--linkedin"></a>
                    <a href="#" class="article__social-link article__social-link--google-plus"></a>
                    <a href="#" class="article__social-link article__social-link--mail"></a>
                </div>
                <h1 class="article__heading">Article Headline one line</h1>
                <div class="article__userName">
					<div class="card__avatar">
						<div class="card__avatar-follow"></div>
					</div>
                    <a href="#">By User Name</a>
                    <time>6 Oct 2015</time>
                </div>
                <button class="button article__follow">Follow +</button>
            </div>

            <div class="article-content-section">
                <div class="content-area">
                    <h1>Australia lose No. 1 Test ranking after 3-0 defeat in Sri Lanka </h1>
                    <p>Australia have conceded the No. 1 Test ranking to India after losing all three Tests on their tour of Sri Lanka. Virat Kohli's team - currently ahead of Pakistan by one point - must win the fourth Test against West Indies, beginning on August 18 in Trinidad, to retain their top ranking.</p>
                    <p>Australia have conceded the No. 1 Test ranking to India after losing all three Tests on their tour of Sri Lanka. Virat Kohli's team - currently ahead of Pakistan by one point - must win the fourth Test against West Indies, beginning on August 18 in Trinidad, to retain their top ranking.</p>
                    <h3>Australia lose No. 1 Test ranking after 3-0 defeat in Sri Lanka </h3>
                    <p>Australia have conceded the No. 1 Test ranking to India after losing all three Tests on their tour of Sri Lanka. Virat Kohli's team - currently ahead of Pakistan by one point - must win the fourth Test against West Indies, beginning on August 18 in Trinidad, to retain their top ranking.</p>
                    <h2>Australia lose No. 1 Test ranking after 3-0 defeat in Sri Lanka </h2>
                    <p>Australia have conceded the No. 1 Test ranking to India after losing all three Tests on their tour of Sri Lanka. Virat Kohli's team - currently ahead of Pakistan by one point - must win the fourth Test against West Indies, beginning on August 18 in Trinidad, to retain their top ranking.</p>

                    <h4>Australia lose No. 1 Test ranking after 3-0 defeat in Sri Lanka </h4>
                    <p>Australia have conceded the No. 1 Test ranking to India after losing all three Tests on their tour of Sri Lanka. Virat Kohli's team - currently ahead of Pakistan by one point - must win the fourth Test against West Indies, beginning on August 18 in Trinidad, to retain their top ranking.</p>
                    <p>Australia have conceded the No. 1 Test ranking to India after losing all three Tests on their tour of Sri Lanka. Virat Kohli's team - currently ahead of Pakistan by one point - must win the fourth Test against West Indies, beginning on August 18 in Trinidad, to retain their top ranking.</p>
                    <h5>Australia lose No. 1 Test ranking after 3-0 defeat in Sri Lanka </h5>
                    <p>Australia have conceded the No. 1 Test ranking to India after losing all three Tests on their tour of Sri Lanka. Virat Kohli's team - currently ahead of Pakistan by one point - must win the fourth Test against West Indies, beginning on August 18 in Trinidad, to retain their top ranking.</p>
                    <h6>Australia lose No. 1 Test ranking after 3-0 defeat in Sri Lanka </h6>
                    <ul>
                        <li>Australia slipped to No. 3 after the 163-run defeat at the SSC; they had begun the series with 118 points but finished the contest just ahead of England, who also have 108 points at No. 4, on the ICC's rankings.</li>
                        <li>As a result of Australia being whitewashed 3-0, Pakistan have risen to No. 2, having drawn their four-Test tour of England 2-2. Should India fail to beat West Indies in Trinidad, Pakistan will become the No. 1 ranked Test side for the first time.</li>
                        <li>Sri Lanka's performance led to them over-taking South Africa to move into sixth place with 95 points. They had begun the home series against Australia at No. 7 with 85 points.</li>
                    </ul>
                    <blockquote cite="http://www.worldwildlife.org/who/index.html">
                    For 50 years, WWF has been protecting the future of nature. The world's leading conservation organization, WWF works in 100 countries and is supported by 1.2 million members in the United States and close to 5 million globally.
                    </blockquote>
                    
                    <div class="medium-insert-embeds">
                        <figure>
                            <div class="medium-insert-embed">
                                <div style="max-width:320px;margin:auto;">
                                    <div class="medium-insert-embeds">
                                        <figure>
                                            <div class="medium-insert-embed">
                                                <div style="max-width:320px;margin:auto;">
                                                     <div>
                                                        <div style="left: 0px; width: 100%; height: 0px; position: relative; padding-bottom: 56.2493%;">
                                                            <iframe frameborder="0" style="top: 0px; left: 0px; width: 100%; height: 100%; position: absolute;" mozallowfullscreen="true" webkitallowfullscreen="true" allowfullscreen="true" src="https://www.youtube.com/embed/QZQ3Bvd6TzQ?wmode=transparent&amp;rel=0&amp;autohide=1&amp;showinfo=0&amp;enablejsapi=1"></iframe>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <figcaption contenteditable="true" class="">Mayank Test</figcaption>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </figure>
                    </div>

                    <!-- Insert Image -->
                    <div class="medium-insert-images medium-insert-active">
                        <figure>
                            <img alt="" src="http://res.cloudinary.com/cognitives/image/upload/v1473425535/msecwvivog78egklce9y.jpg">
                            <figcaption class="">Mayank Test</figcaption>
                        </figure>
                    </div>


                    <!-- Insert Image Slider -->
                    <div class="medium-insert-images medium-insert-active">
                        <?php require("partials/_slides.php") ?>
                    </div>

                    <!-- Insert Image Slider -->
                    <div class="medium-insert-images">
                        <?php require("partials/_slides.php") ?>
                    </div>


                    <!-- Insert Video -->
                    <div class="medium-insert-vimeo">
                        <figure>
                            <div class="medium-insert-vim">
                                <div style="max-width:320px;margin:auto;">

                                <!-- You're using demo endpoint of Iframely API commercially. Max-width is limited to 320px. Please get your own API key at https://iframely.com. -->

                                    <div>
                                        <div style="left: 0px; width: 100%; height: 0px; position: relative; padding-bottom: 56.2493%;">
                                            <iframe frameborder="0" style="top: 0px; left: 0px; width: 100%; height: 100%; position: absolute;" mozallowfullscreen="true" webkitallowfullscreen="true" allowfullscreen="true" src="https://player.vimeo.com/video/181586390?byline=0&amp;badge=0&amp;portrait=0&amp;title=0"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <figcaption contenteditable="true" class="">Mayank Test</figcaption>
                        </figure>
                    </div>

                    <!-- Insta Post -->
                    <div class="medium-insert-instagram">
                        <figure>
                            <div class="medium-insert-insta">
                                <div data-embed-code="<div style=&quot;max-width:320px;margin:auto;&quot;>

                                        <!-- You're using demo endpoint of Iframely API commercially. Max-width is limited to 320px. Please get your own API key at https://iframely.com. -->

                                        <blockquote class=&quot;instagram-media&quot; data-instgrm-version=&quot;7&quot; style=&quot; background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);&quot;><div style=&quot;padding:8px;&quot;> <div style=&quot; background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;&quot;> <div style=&quot; background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;&quot;></div></div><p style=&quot; color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;&quot;><a href=&quot;https://www.instagram.com/p/BKHNsmwjC0s/&quot; style=&quot; color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;&quot; target=&quot;_blank&quot;>A photo posted by Cricket Wireless (@cricketnation)</a> on <time style=&quot; font-family:Arial,sans-serif; font-size:14px; line-height:17px;&quot; datetime=&quot;2016-09-08T23:04:33+00:00&quot;>Sep 8, 2016 at 4:04pm PDT</time></p></div></blockquote>
                                        <script async defer src=&quot;//platform.instagram.com/en_US/embeds.js&quot;></script>
                                    </div>">
                                    <div style="max-width:320px;margin:auto;">

                                        <!-- You're using demo endpoint of Iframely API commercially. Max-width is limited to 320px. Please get your own API key at https://iframely.com. -->

                                        <blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);">
                                            <div style="padding:8px;">
                                                <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;">
                                                    <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div>
                                                </div>
                                                <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BKHNsmwjC0s/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A photo posted by Cricket Wireless (@cricketnation)</a>                                            on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2016-09-08T23:04:33+00:00">Sep 8, 2016 at 4:04pm PDT</time></p>
                                            </div>
                                        </blockquote>
                                        <script async="" defer="" src="//platform.instagram.com/en_US/embeds.js"></script>
                                    </div>
                                </div>
                            </div>
                        </figure>
                    </div>

                    <!-- Twitter -->
                    <div class="medium-insert-twitter">
                        <figure>
                            <div class="medium-insert-tweet">
                                <div data-embed-code="<div style=&quot;max-width:320px;margin:auto;&quot;>

                                    <!-- You're using demo endpoint of Iframely API commercially. Max-width is limited to 320px. Please get your own API key at https://iframely.com. -->

                                    <blockquote class=&quot;twitter-tweet&quot; align=&quot;center&quot;><p lang=&quot;en&quot; dir=&quot;ltr&quot;>Soaking in all the goodness of the sun &amp;amp; the sea,one last stroll before we bid goodbye.Maldives you will be missed! <a href=&quot;https://t.co/1iMpnjVJq3&quot;>pic.twitter.com/1iMpnjVJq3</a></p>&amp;mdash; Akshay Kumar (@akshaykumar) <a href=&quot;https://twitter.com/akshaykumar/status/775284610020151296&quot;>September 12, 2016</a></blockquote>
                                    <script async src=&quot;//platform.twitter.com/widgets.js&quot; charset=&quot;utf-8&quot;></script>
                                    </div>">
                                    <div style="max-width:320px;margin:auto;">

                                        <!-- You're using demo endpoint of Iframely API commercially. Max-width is limited to 320px. Please get your own API key at https://iframely.com. -->

                                        <blockquote class="twitter-tweet" align="center">
                                            <p dir="ltr" lang="en">Soaking in all the goodness of the sun &amp; the sea,one last stroll before we bid goodbye.Maldives you will be missed! <a href="https://t.co/1iMpnjVJq3">pic.twitter.com/1iMpnjVJq3</a></p>— Akshay Kumar (@akshaykumar)
                                            <a href="https://twitter.com/akshaykumar/status/775284610020151296">September 12, 2016</a></blockquote>
                                        <script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                                    </div>
                                </div>
                            </div>
                        </figure>
                    </div>


                    <!-- Insert Facebook -->
                    <div class="medium-insert-embeds">
                        <figure>
                            <div class="medium-insert-embed">
                                <div style="max-width:320px;margin:auto;">

                                    <!-- You're using demo endpoint of Iframely API commercially. Max-width is limited to 320px. Please get your own API key at https://iframely.com. -->

                                    <div>
                                        <div style="left: 0px; width: 100%; height: 0px; position: relative; padding-bottom: 56.2493%;"><iframe src="https://player.vimeo.com/video/72466668?byline=0&amp;badge=0&amp;portrait=0&amp;title=0" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" style="top: 0px; left: 0px; width: 100%; height: 100%; position: absolute;"
                                                frameborder="0"></iframe></div>
                                    </div>
                                </div>
                            </div>
                        </figure>
                    </div>


                    <!-- Insert Facebook -->
                    <div class="medium-insert-embeds">
                        <figure>
                            <div class="medium-insert-embed">
                                <div data-embed-code="<a href=&quot;https://www.facebook.com/amandipsinghdhaliwal/posts/1174332352603039&quot; data-iframely-url=&quot;//cdn.iframe.ly/IORsnJ&quot; data-template=&quot;inline&quot;></a><script async src=&quot;//cdn.iframe.ly/embed.js&quot; charset=&quot;utf-8&quot;></script>">
                                    <a href="https://www.facebook.com/amandipsinghdhaliwal/posts/1174332352603039" data-iframely-url="//cdn.iframe.ly/IORsnJ" data-template="inline"></a>
                                    <script async="" src="//cdn.iframe.ly/embed.js" charset="utf-8"></script>
                                </div>
                            </div>
                        </figure>
                    </div>

                    <div class="medium-insert-embeds">
                        <figure>
                            <div class="medium-insert-embed">
                                <div style="max-width:320px;margin:auto;">

                                    <!-- You're using demo endpoint of Iframely API commercially. Max-width is limited to 320px. Please get your own API key at https://iframely.com. -->

                                    <div>
                                        <div style="left: 0px; width: 100%; height: 0px; position: relative; padding-bottom: 56.2493%;"><iframe src="https://www.youtube.com/embed/xiwlTvpSlRQ?wmode=transparent&amp;rel=0&amp;autohide=1&amp;showinfo=0&amp;enablejsapi=1" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" style="top: 0px; left: 0px; width: 100%; height: 100%; position: absolute;"
                                                frameborder="0"></iframe></div>
                                    </div>
                                </div>
                            </div>
                        </figure>
                    </div>

                    <div class="medium-insert-embeds">
                        <figure>
                            <div class="medium-insert-embed">
                                <div data-embed-code="<blockquote class=&quot;twitter-tweet&quot; data-lang=&quot;en&quot;><p lang=&quot;en&quot; dir=&quot;ltr&quot;><a href=&quot;https://twitter.com/hashtag/OnThisDay?src=hash&quot;>#OnThisDay</a> in 2007, the inaugural World Twenty20 kicked off in South Africa! What&amp;#39;s your favourite <a href=&quot;https://twitter.com/hashtag/WT20?src=hash&quot;>#WT20</a> memory? <a href=&quot;https://t.co/yDeewPRRbE&quot;>pic.twitter.com/yDeewPRRbE</a></p>&amp;mdash; ICC (@ICC) <a href=&quot;https://twitter.com/ICC/status/774834879590653952&quot;>September 11, 2016</a></blockquote><script async src=&quot;//platform.twitter.com/widgets.js&quot; charset=&quot;utf-8&quot;></script>">
                                    <blockquote class="twitter-tweet" data-lang="en">
                                        <p dir="ltr" lang="en"><a href="https://twitter.com/hashtag/OnThisDay?src=hash">#OnThisDay</a> in 2007, the inaugural World Twenty20 kicked off in South Africa! What's your favourite <a href="https://twitter.com/hashtag/WT20?src=hash">#WT20</a>                                    memory? <a href="https://t.co/yDeewPRRbE">pic.twitter.com/yDeewPRRbE</a></p>— ICC (@ICC) <a href="https://twitter.com/ICC/status/774834879590653952">September 11, 2016</a></blockquote>
                                    <script async="" src="//platform.twitter.com/widgets.js"
                                        charset="utf-8"></script>
                                </div>
                            </div>
                        </figure>
                    </div>
                </div>
            </div>

            <!-- Disqus Comment Added -->
            <div class="DisqusCommentArea">
                <div class="article-content-section">
                    <div class="content-area">
                        <div class="medium-insert-embeds">
                            <div id="disqus_thread" class="DisqusPlaceholder"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

/**
 *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
 *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables */
/*
var disqus_config = function () {
    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
    var d = document, s = d.createElement('script');
    s.src = '//cognitives.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<script id="dsq-count-scr" src="//cognitives.disqus.com/count.js" async></script>

<?php require("partials/_follow.php") ?>
<?php require("partials/_footer.php") ?>
