<?php require("partials/_header.php") ?>
<div class="page_section cards_main home_section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="col-full">
                    <a href="#" class="card card__news ad_icon content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>


                <div class="col-third">
                    <a href="#" class="card card__twitter withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-third">
                    <a href="#" class="card card__news withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-third">
                    <a href="#" class="card card__youtube withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>


                <div class="col-half">
                    <a href="#" class="card card__news content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-half">
                    <a href="#" class="card card__twitter content_overlay_image">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>


                <div class="col-quarter">
                    <a href="#" class="card card__twitter withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-quarter">
                    <a href="#" class="card card__facebook withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-quarter">
                    <a href="#" class="card card__news ad_icon withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
                <div class="col-quarter">
                    <a href="#" class="card card__youtube withImage__content">
                        <?php require "partials/cards/final/_content.php"; ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer__like__section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="section-heading">
                    <h2>You might also like...</h2>
                </div>
                <div class="cards_main">
                    <div class="col-third">
                        <a href="#" class="card card__news withImage__content">
                            <?php require "partials/cards/final/_content.php"; ?>
                        </a>
                    </div>
                    <div class="col-third">
                        <a href="#" class="card card__news withImage__content">
                            <?php require "partials/cards/final/_content.php"; ?>
                        </a>
                    </div>
                    <div class="col-third">
                        <a href="#" class="card card__news withImage__content">
                            <?php require "partials/cards/final/_content.php"; ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require("partials/_footer.php") ?>
