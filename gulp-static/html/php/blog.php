<?php require("partials/_header.php") ?>
<div class="page_section home_section blog_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="blog_top_section">
                    <div class="blog-header image-covered" style="background-image:url('https://placeholdit.imgix.net/~text?txtsize=75&bg=e8117f&txtclr=ffffff&txt=1000%C3%971000&w=1000&h=1000')">
                        <div class="blog-content">
                            <h2 class="heading-section">Blog Title</h2>
                            <p>By <a href="javascript:;">User Name</a></p>
                        </div>
                        <div class="blog-userprofile image-covered" style="background-image:url('https://placeholdit.imgix.net/~text?txtsize=75&bg=f00&txtclr=ffffff&txt=1000%C3%971000&w=1000&h=1000')"></div>
                    </div>
                    <div class="social-section">
                        <div class="social-connect">
                            <ul>
                                <li><a class="twitter" href="javascript:;"><i class="fa fa-twitter"></i></a></li>
                                <li><a class="facebook" href="javascript:;"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="google-plus" href="javascript:;"><i class="fa fa-google-plus"></i></a></li>
                                <li><a class="linkedin" href="javascript:;"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                        <div class="social-button">
                            <a href="javascript:;" class="button upper black">Follow <i class="fa fa-plus"></i></a>
                        </div>

                    </div>
                </div>
                <div class="cards_main">
                    <div class="col-third">
                        <a href="#" class="card card__twitter withImage__content">
                            <?php require "partials/cards/final/_content.php"; ?>
                        </a>
                    </div>
                    <div class="col-third">
                        <a href="#" class="card card__news withImage__content">
                            <?php require "partials/cards/final/_content.php"; ?>
                        </a>
                    </div>
                    <div class="col-third">
                        <a href="#" class="card card__youtube withImage__content">
                            <?php require "partials/cards/final/_content.php"; ?>
                        </a>
                    </div>


                    <div class="col-half">
                        <a href="#" class="card card__news content_overlay_image">
                            <?php require "partials/cards/final/_content.php"; ?>
                        </a>
                    </div>
                    <div class="col-half">
                        <a href="#" class="card card__twitter content_overlay_image">
                            <?php require "partials/cards/final/_content.php"; ?>
                        </a>
                    </div>


                    <div class="col-third">
                        <a href="#" class="card card__twitter withImage__content">
                            <?php require "partials/cards/final/_content.php"; ?>
                        </a>
                    </div>
                    <div class="col-third">
                        <a href="#" class="card card__news withImage__content">
                            <?php require "partials/cards/final/_content.php"; ?>
                        </a>
                    </div>
                    <div class="col-third">
                        <a href="#" class="card card__facebook withImage__content">
                            <?php require "partials/cards/final/_content.php"; ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer__like__section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="section-heading">
                    <h2>You might also like...</h2>
                </div>
                <div class="cards_main">
                    <div class="col-third">
                        <a href="#" class="card card__news withImage__content">
                            <?php require "partials/cards/final/_content.php"; ?>
                        </a>
                    </div>
                    <div class="col-third">
                        <a href="#" class="card card__news withImage__content">
                            <?php require "partials/cards/final/_content.php"; ?>
                        </a>
                    </div>
                    <div class="col-third">
                        <a href="#" class="card card__news withImage__content">
                            <?php require "partials/cards/final/_content.php"; ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require("partials/_footer.php") ?>
